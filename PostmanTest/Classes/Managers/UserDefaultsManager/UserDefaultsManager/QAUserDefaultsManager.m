//
//  QAUserDefailtsManager.m
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAUserDefaultsManager.h"
#import "QAUserDefaults.h"

NSString * const AccessTokenKey = @"accessToken";

@interface QAUserDefaultsManager ()

@property (nonatomic, strong) id<QALocalStorageProtocol> userDefaults;

@end

@implementation QAUserDefaultsManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setUserDefaults:[[QAUserDefaults alloc] init]];
    }
    return self;
}

- (void)saveToken:(NSString *)object
{
    [[self userDefaults] saveObject:object forKey:AccessTokenKey];
}

- (NSString *)getToken
{
    return [[self userDefaults] getObjectForKey:AccessTokenKey];
}

- (void)resetToken
{
    [[self userDefaults] resetObjectforKey:AccessTokenKey];
}

@end
