//
//  QAUserDefailtsManager.h
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QAUserDefaultsManager : NSObject

- (void)saveToken:(NSString *)object;
- (NSString *)getToken;
- (void)resetToken;

@end
