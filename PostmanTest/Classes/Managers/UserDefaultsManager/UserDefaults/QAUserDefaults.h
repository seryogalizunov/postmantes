//
//  QAUserDefailts.h
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QALocalStorageProtocol.h"

@interface QAUserDefaults : NSObject <QALocalStorageProtocol>

@end
