//
//  QADataManager.h
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QADataManagerProtocol.h"
#import "QAUserDefaultsManager.h"

@interface QADataManager : NSObject <QADataManagerProtocol>

@end
