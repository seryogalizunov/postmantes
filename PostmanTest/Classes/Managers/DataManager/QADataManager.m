//
//  QADataManager.m
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QADataManager.h"
#import "QAServerSideManager.h"
#import "QACoreDataManager.h"

#import "QAPostRegisterQuery.h"
#import "QAPostLoginQuery.h"
#import "QAPostLogoutQuery.h"
#import "QAGetTokenQuery.h"
#import "QAGetCaterogiesQuery.h"
#import "QAGetCategoryWithQuestionsQuery.h"
#import "QAPostHistoryQuery.h"
#import "QAGetHistoryQuery.h"
#import "QAGetCaterogiesQuery.h"
#import "QAGetChampionshipQuery.h"
#import "QAHistories.h"
#import "QAHistoriesCoreDataModel.h"
#import "QAUserDefaultsManager.h"
#import "QAToken.h"

#import "QANetworkConfiguration.h"
#import "QANetworkClient.h"
#import "QAReachability.h"
#import "QAHistoryCoreDataModel.h"
#import "QAResultCoreDataModel.h"
#import "NSDate+NSDateCategory.h"

@interface QADataManager ()

@property (nonatomic, strong) QAServerSideManager *serverManager;
@property (nonatomic, strong) QACoreDataManager *coreDataManager;
@property (nonatomic, strong) QAUserDefaultsManager *userDefaultsManager;

@end

@implementation QADataManager

#pragma mark -
#pragma mark - Init Managers

- (instancetype)initWithServerSideManager:(QAServerSideManager *)serverSideManager
                          coreDataManager:(QACoreDataManager *)coreDataManager
                       userDefaultManager:(QAUserDefaultsManager *)userDefaultManager
{
    self = [super init];
    if (self)
    {
        [self setServerManager:serverSideManager];
        [self setCoreDataManager:coreDataManager];
        [self setUserDefaultsManager:userDefaultManager];
    }
    return self;
}

#pragma mark -
#pragma mark - Post Register

- (void)postRegisterWithQuery:(QAPostRegisterQuery *)query
                 successBlock:(ServerSideManagerSuccessBlock)success
                 failureBlock:(ServerSideManagerFailureBlock)failure
{
    
    [[self serverManager] postRegisterWithQuery:query
                                   successBlock:success
                                   failureBlock:failure];
}

#pragma mark -
#pragma mark - Post Login

- (void)postLoginWithQuery:(QAPostLoginQuery *)query
              successBlock:(ServerSideManagerSuccessBlock)success
              failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] postLoginWithQuery:query
                                successBlock:^(id responseObject)
                                {
                                    QAToken *tokenModel = responseObject[NSStringFromClass([QAToken class])];
                                    [[self userDefaultsManager] saveToken:[tokenModel token]];
                                    success(responseObject);
                                }
                                failureBlock:failure];
}

#pragma mark -
#pragma mark - Post Logout

- (void)postLogoutWithQuery:(QAPostLogoutQuery *)query
               successBlock:(ServerSideManagerSuccessBlock)success
               failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] postLogoutWithQuery:query
                                 successBlock:^(id responseObject)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        QAHistoriesCoreDataModel *historiesCoreDataModel = [[weakSelf coreDataManager] getHistoriesWithToken:[[weakSelf userDefaultsManager] getToken]];
                                        [[weakSelf coreDataManager] deleteHistoryCoreDataModel:historiesCoreDataModel];
                                        [[weakSelf userDefaultsManager] resetToken];
                                        success(responseObject);
                                    }
                                 failureBlock:failure];
}

#pragma mark -
#pragma mark - Get Token

- (void)getTokenWithQuery:(QAGetTokenQuery *)query
             successBlock:(ServerSideManagerSuccessBlock)success
             failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] getTokenWithQuery:query
                               successBlock:success
                               failureBlock:failure];
}

#pragma mark -
#pragma mark - Get Catigories

- (void)getCaterogiesWithQuery:(QAGetCaterogiesQuery *)query
                  successBlock:(ServerSideManagerSuccessBlock)success
                  failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] getCaterogiesWithQuery:query
                                    successBlock:success
                                    failureBlock:failure];
}

#pragma mark -
#pragma mark - Get GategoryQuestions

- (void)getCaterogyQuestionsWithQuery:(QAGetCategoryWithQuestionsQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] getCaterogyQuestionsWithQuery:query
                                           successBlock:success
                                           failureBlock:failure];
}

#pragma mark -
#pragma mark - Post History

- (void)postHistoryQuestionsWithQuery:(QAPostHistoryQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] postHistoryQuestionsWithQuery:query
                                           successBlock:success
                                           failureBlock:failure];
}

#pragma mark -
#pragma mark - Get History

- (void)getHistoryQuestionsWithQuery:(QAGetHistoryQuery *)query
                        successBlock:(ServerSideManagerSuccessBlock)success
                        failureBlock:(ServerSideManagerFailureBlock)failure
{
    if ([QAReachability hasConnectivity] == YES)
    {
        [[self serverManager] getHistoryQuestionsWithQuery:query
                                              successBlock:^(id responseObject)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf saveHistories:responseObject];
                                                    success(responseObject);
                                                }
                                              failureBlock:failure];
    }
    else
    {
        success([[self coreDataManager] convertHistoriesCoreDataModelInHistoriesNetworkModel:[[self coreDataManager] getHistoriesWithToken:[[self userDefaultsManager] getToken]]]);
    }
}

#pragma mark -
#pragma mark - Get Championship

- (void)getChampionshipQuestionsWithQuery:(QAGetChampionshipQuery *)query
                             successBlock:(ServerSideManagerSuccessBlock)success
                             failureBlock:(ServerSideManagerFailureBlock)failure
{
    [[self serverManager] getChampionshipQuestionsWithQuery:query
                                               successBlock:success
                                               failureBlock:failure];
}

#pragma mark -
#pragma mark - Save History

- (void)saveHistories:(id)networkModel
{
    QAHistoriesCoreDataModel *historiesCoreDataModel = [[self coreDataManager] getHistoriesWithToken:[[self userDefaultsManager] getToken]];
    
    if (historiesCoreDataModel != nil)
    {
        [[self coreDataManager] deleteHistoryCoreDataModel:historiesCoreDataModel];
        [[self coreDataManager] saveHistoriesInCoreDataModel:networkModel
                                              andAccessToken:[[self userDefaultsManager] getToken]];
    }
    else
    {
        [[self coreDataManager] saveHistoriesInCoreDataModel:networkModel
                                              andAccessToken:[[self userDefaultsManager] getToken]];
    }
}

@end
