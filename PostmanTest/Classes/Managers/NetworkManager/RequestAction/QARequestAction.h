//
//  QARequestAction.h
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, QAHTTPMethod) {
    GET,
    POST
};

@interface QARequestAction : NSObject

@property (nonatomic, assign) QAHTTPMethod requestMethod;
@property (nonatomic, copy) NSString *requestAction;
@property (nonatomic, copy) NSDictionary *requestParameters;

@end
