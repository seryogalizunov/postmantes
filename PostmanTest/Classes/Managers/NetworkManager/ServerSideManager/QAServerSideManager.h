//
//  QAServerSideManager.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAServerSideManagerProtocol.h"

@interface QAServerSideManager : NSObject <QAServerSideManagerProtocol>

@end
