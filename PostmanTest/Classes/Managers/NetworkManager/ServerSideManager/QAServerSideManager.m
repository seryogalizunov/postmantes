//
//  QAServerSideManager.m
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAServerSideManager.h"
#import "QABaseCommand.h"
#import "QAGetTokenCommand.h"
#import "QAPostRegisterCommand.h"
#import "QAPostLoginCommand.h"
#import "QAGetCaterogiesCommand.h"
#import "QANetworkClient.h"
#import "QAGetTokenQuery.h"
#import "QAPostRegisterQuery.h"
#import "QAPostLoginQuery.h"
#import "QAGetCaterogiesQuery.h"
#import "QAGetCategoryWithQuestionsQuery.h"
#import "QAGetCategoryWithQuestionsCommand.h"
#import "QAPostLogoutQuery.h"
#import "QAPostLogoutCommand.h"
#import "QAPostHistoryQuery.h"
#import "QAPostHistoryCommand.h"
#import "QAGetHistoryQuery.h"
#import "QAGetHistoryCommand.h"
#import "QAGetChampionshipQuery.h"
#import "QAGetChampionshipCommand.h"
#import "QAPostLogoutQuery.h"
#import "QAPostHistoryQuery.h"

@interface QAServerSideManager ()

@property (nonatomic, strong) QANetworkClient *networkCient;
@property (nonatomic, strong) NSOperationQueue *operationQueue;

@end

@implementation QAServerSideManager

#pragma mark -
#pragma mark - Init NetworkClient

- (instancetype)initWithNetworkClient:(QANetworkClient *)networkClient
{
    self = [super init];
    if (self)
    {
        [self setOperationQueue:[[NSOperationQueue alloc] init]];
        [[self operationQueue] setMaxConcurrentOperationCount:5];
        [self setNetworkCient:networkClient];
    }
    return self;
}

- (void)enqueuCommand:(QABaseCommand *)command
{
    [[self operationQueue] addOperation:command];
}

#pragma mark -
#pragma mark - Post Register

- (void)postRegisterWithQuery:(QAPostRegisterQuery *)query
                 successBlock:(ServerSideManagerSuccessBlock)success
                 failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAPostRegisterCommand *postRegister = [[QAPostRegisterCommand alloc] initWithClient:[self networkCient]
                                                                                  query:query
                                                                           successBlock:success
                                                                           failureBlock:failure];
    [self enqueuCommand:postRegister];
}

#pragma mark -
#pragma mark - Post Login

- (void)postLoginWithQuery:(QAPostLoginQuery *)query
              successBlock:(ServerSideManagerSuccessBlock)success
              failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAPostLoginCommand *postLogin = [[QAPostLoginCommand alloc] initWithClient:[self networkCient]
                                                                         query:query
                                                                  successBlock:success
                                                                  failureBlock:failure];
    [self enqueuCommand:postLogin];
}

#pragma mark -
#pragma mark - Post Logout

- (void)postLogoutWithQuery:(QAPostLogoutQuery *)query
               successBlock:(ServerSideManagerSuccessBlock)success
               failureBlock:(ServerSideManagerFailureBlock)failure;
{
    QAPostLogoutCommand *postLogout = [[QAPostLogoutCommand alloc] initWithClient:[self networkCient]
                                                                            query:query
                                                                     successBlock:success
                                                                     failureBlock:failure];
    [self enqueuCommand:postLogout];
}

#pragma mark -
#pragma mark - Get Token

- (void)getTokenWithQuery:(QAGetTokenQuery *)query
             successBlock:(ServerSideManagerSuccessBlock)success
             failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAGetTokenCommand *getTokenCommand = [[QAGetTokenCommand alloc] initWithClient:[self networkCient]
                                                                             query:query
                                                                      successBlock:success
                                                                      failureBlock:failure];
    [self enqueuCommand:getTokenCommand];
}

#pragma mark -
#pragma mark - Get Catigories

- (void)getCaterogiesWithQuery:(QAGetCaterogiesQuery *)query
                  successBlock:(ServerSideManagerSuccessBlock)success
                  failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAGetCaterogiesCommand *getCategory = [[QAGetCaterogiesCommand alloc] initWithClient:[self networkCient]
                                                                                   query:query
                                                                            successBlock:success
                                                                            failureBlock:failure];
    [self enqueuCommand:getCategory];
}

#pragma mark -
#pragma mark - Get GategoryQuestions

- (void)getCaterogyQuestionsWithQuery:(QAGetCategoryWithQuestionsQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAGetCategoryWithQuestionsCommand *getCategoryQuestions = [[QAGetCategoryWithQuestionsCommand alloc] initWithClient:[self networkCient]
                                                                                                                  query:query
                                                                                                           successBlock:success
                                                                                                           failureBlock:failure];
    [self enqueuCommand:getCategoryQuestions];
}

#pragma mark -
#pragma mark - Post History

- (void)postHistoryQuestionsWithQuery:(QAPostHistoryQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAPostHistoryCommand *postHistory = [[QAPostHistoryCommand alloc] initWithClient:[self networkCient]
                                                                               query:query
                                                                        successBlock:success
                                                                        failureBlock:failure];
    [self enqueuCommand:postHistory];
}

#pragma mark -
#pragma mark - Get History

- (void)getHistoryQuestionsWithQuery:(QAGetHistoryQuery *)query
                        successBlock:(ServerSideManagerSuccessBlock)success
                        failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAGetHistoryCommand *getHistory = [[QAGetHistoryCommand alloc] initWithClient:[self networkCient]
                                                                            query:query
                                                                     successBlock:success
                                                                     failureBlock:failure];
    [self enqueuCommand:getHistory];
}

#pragma mark -
#pragma mark - Get Championship

- (void)getChampionshipQuestionsWithQuery:(QAGetChampionshipQuery *)query
                             successBlock:(ServerSideManagerSuccessBlock)success
                             failureBlock:(ServerSideManagerFailureBlock)failure
{
    QAGetChampionshipCommand *getChampionship = [[QAGetChampionshipCommand alloc] initWithClient:[self networkCient]
                                                                                           query:query
                                                                                    successBlock:success
                                                                                    failureBlock:failure];
    [self enqueuCommand:getChampionship];
}

@end
