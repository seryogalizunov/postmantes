//
//  QABaseCommand.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QANetworkClient.h"
#import "QAConectionCommandProtocol.h"

typedef void (^CommandSuccessBlock)(id responseObject);
typedef void (^CommandFailureBlock)(QAError *theError);

@interface QABaseCommand : NSOperation
{
    BOOL executing;
    BOOL finished;
}

@property (nonatomic, strong) CommandSuccessBlock successBlock;
@property (nonatomic, strong) CommandFailureBlock failureBlock;

- (instancetype)initWithClient:(QANetworkClient *)client
                  successBlock:(CommandSuccessBlock)success
                  failureBlock:(CommandFailureBlock)failure;

@end
