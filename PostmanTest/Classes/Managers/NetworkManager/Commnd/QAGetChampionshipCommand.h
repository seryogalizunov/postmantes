//
//  QAGetChampionshipCommand.h
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"

@class QAGetChampionshipQuery;

@interface QAGetChampionshipCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetChampionshipQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
