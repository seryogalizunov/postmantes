//
//  QAPostHistoryCommand.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"

@class QAPostHistoryQuery;

@interface QAPostHistoryCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostHistoryQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
