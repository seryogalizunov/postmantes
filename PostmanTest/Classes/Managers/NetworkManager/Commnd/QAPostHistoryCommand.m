//
//  QAPostHistoryCommand.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAPostHistoryCommand.h"
#import "QAPostHistoryQuery.h"

static NSString *const QAPostHistoryCommandPathString = @"history";


@interface QAPostHistoryCommand ()

@property (nonatomic, strong) QAPostHistoryQuery* query;

@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, assign) NSTimeInterval date;
@property (nonatomic, copy) NSArray *questionsArray;

@end

@implementation QAPostHistoryCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostHistoryQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
        [self setCategoryName:[query categoryName]];
        [self setDate:[query dateInTimeInterval]];
        [self setQuestionsArray:[query questionsArray]];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@([self date]), @"date", [self categoryName], @"name", [self questionsArray], @"questions", nil];
    NSDictionary *resultParams = [NSDictionary dictionaryWithObjectsAndKeys:params, @"data", nil];
    
    [action setRequestAction:QAPostHistoryCommandPathString];
    [action setRequestParameters:resultParams];
    [action setRequestMethod:POST];
    
    return action;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    return @{};
}

@end
