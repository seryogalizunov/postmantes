//
//  QAGetTokenCommand.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
@class QANetworkClient;
@class QAGetTokenQuery;

@interface QAGetTokenCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetTokenQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
