//
//  QAGetCategoryWithQuestionsCommand.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
#import "QAGetCategoryWithQuestionsQuery.h"

@interface QAGetCategoryWithQuestionsCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetCategoryWithQuestionsQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
