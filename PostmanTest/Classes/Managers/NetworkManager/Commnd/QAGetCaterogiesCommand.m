//
//  QAGetCaterogyWithNameCommand.m
//  QuizApp
//
//  Created by Lizunov on 10/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAGetCaterogiesCommand.h"
#import "QAGetCaterogiesQuery.h"
#import "QACategory.h"
#import "QACategories.h"

static NSString *const QAGetCaterogiesCommandPathString = @"category";


@interface QAGetCaterogiesCommand ()

@property (nonatomic, strong) QAGetCaterogiesQuery* query;

@end

@implementation QAGetCaterogiesCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetCaterogiesQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = nil;
    
    [action setRequestAction:QAGetCaterogiesCommandPathString];
    [action setRequestParameters:params];
    [action setRequestMethod:GET];
    
    return action;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    NSMutableArray *categoriesArray = [[NSMutableArray alloc] init];
    
    for (NSInteger index = 0; index < [responceObject count]; index++)
    {
        QACategory *category = [[QACategory alloc] initWithCategoryName:[[responceObject objectAtIndex:index] objectForKey:@"categoryName"]
                                                               objectId:[[responceObject objectAtIndex:index] objectForKey:@"objectId"]];
        [categoriesArray addObject:category];
    }
    QACategories *categoriesModel = [[QACategories alloc] initWithCategoryArray:categoriesArray];
    return @{NSStringFromClass([QACategories class]): categoriesModel};
}

@end
