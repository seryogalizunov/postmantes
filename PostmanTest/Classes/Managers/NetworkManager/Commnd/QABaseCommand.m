//
//  QABaseCommand.m
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
#import "QARequestAction.h"
#import "QABaseCommand__Protected.h"

@interface QABaseCommand ()

@property (atomic, assign) BOOL _executing;
@property (atomic, assign) BOOL _finished;

@end

@implementation QABaseCommand

#pragma mark -
#pragma mark - Init

- (instancetype)initWithClient:(QANetworkClient *)client
                  successBlock:(CommandSuccessBlock)success
                  failureBlock:(CommandFailureBlock)failure
{
    self = [super init];
    if (self)
    {
        [self setNetworkClient:client];
        [self setSuccessBlock:success];
        [self setFailureBlock:failure];
    }
    return self;
}

#pragma mark -
#pragma mark - NSOperation Methods

- (void) start;
{
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    
    QARequestAction *action = [self createAction];
    [self performRequestWithAction:action];
    
    self._executing = YES;
    self._finished = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (void) main;
{
    if ([self isCancelled])
    {
        return;
    }
}

- (BOOL) isAsynchronous;
{
    return YES;
}

- (BOOL)isExecuting
{
    return self._executing;
}

- (BOOL)isFinished
{
    return self._finished;
}

- (void)completeOperation
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    self._executing = NO;
    self._finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

#pragma mark -
#pragma mark - Perform Request With Action

- (void)performRequestWithAction:(QARequestAction *)action
{    
    [[self networkClient] sendRequestWithAction:action
                                   successBlock:^(id responseObject)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        weakSelf.successBlock([weakSelf parseResponceObject:responseObject]);
                                    }
                                   failureBlock:^(QAError *theError)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        weakSelf.failureBlock(theError);
                                    }];
}

@end
