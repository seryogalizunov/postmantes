//
//  QAPostLogoutCommand.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAPostLogoutCommand.h"
#import "QAPostLogoutQuery.h"

static NSString *const QAPostLogoutCommandPathString = @"logout";


@interface QAPostLogoutCommand ()

@property (nonatomic, strong) QAPostLogoutQuery* query;

@end

@implementation QAPostLogoutCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostLogoutQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = nil;
    
    [action setRequestAction:QAPostLogoutCommandPathString];
    [action setRequestParameters:params];
    [action setRequestMethod:POST];
    
    return action;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    return @{};
}

@end
