//
//  QAGetCategoryWithQuestionsCommand.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAGetCategoryWithQuestionsCommand.h"
#import "QAGetCategoryWithQuestionsQuery.h"
#import "QACategoryQuestions.h"
#import "QAQuestion.h"
#import "QAAnswer.h"


static NSString *const QAGetCaterogiesCommandPathString = @"category";


@interface QAGetCategoryWithQuestionsCommand ()

@property (nonatomic, strong) QAGetCategoryWithQuestionsQuery* query;

@end

@implementation QAGetCategoryWithQuestionsCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetCategoryWithQuestionsQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = nil;
    
    [action setRequestAction:[self URLstringWithPath:QAGetCaterogiesCommandPathString
                                               where:[[self query] where]
                                              offset:[[self query] offset]
                                            pageSize:[[self query] pageSize]]];
    [action setRequestParameters:params];
    [action setRequestMethod:GET];
    
    return action;
}

- (NSString *)URLstringWithPath:(NSString *)path
                          where:(NSString *)where
                         offset:(NSString *)offset
                       pageSize:(NSString *)pageSize
{
    NSString *resultString = [NSString stringWithFormat:@"%@?where=%@&pageSize=%@&offset=%@", path, where, pageSize, offset];
    return resultString;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    NSMutableArray *categoriesArray = [[NSMutableArray alloc] init];
    
    for (NSInteger indexQuestions = 0; indexQuestions < [responceObject count]; indexQuestions++)
    {
        NSMutableArray *resultAnswerArray = [[NSMutableArray alloc] init];
        NSArray *answersArray = [[responceObject objectAtIndex:indexQuestions] objectForKey:@"answers"];
        
        for (NSInteger indexAnswer = 0; indexAnswer < [answersArray count]; indexAnswer++)
        {
            QAAnswer *answer = [[QAAnswer alloc] initWithAnswer:[answersArray objectAtIndex:indexAnswer]];
            [resultAnswerArray addObject:answer];
        }
        QAQuestion *questionModel = [[QAQuestion alloc] initWithAnswersArray:resultAnswerArray
                                                                  categoryId:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"categoryId"]
                                                                    objectId:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"objectId"]
                                                                    question:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"question"]
                                                                 rightAnswer:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"rightAnswer"]];
        [categoriesArray addObject:questionModel];
    }
    QACategoryQuestions *categoryQuestionsModel = [[QACategoryQuestions alloc] initWithQuestionsArray:categoriesArray];    
    return @{NSStringFromClass([QACategoryQuestions class]): categoryQuestionsModel};
}

@end


