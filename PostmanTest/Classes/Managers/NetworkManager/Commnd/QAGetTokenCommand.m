//
//  QAGetTokenCommand.m
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAGetTokenCommand.h"
#import "QABaseCommand__Protected.h"
#import "QANetworkClient.h"
#import "QAGetTokenQuery.h"

static NSString *const QAGetTokenCommandPathString = @"token";

@interface QAGetTokenCommand ()

@property (nonatomic, strong) QAGetTokenQuery *query;

@end

@implementation QAGetTokenCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetTokenQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[[self query] token], @"token", nil];
    
    [action setRequestAction:QAGetTokenCommandPathString];
    [action setRequestParameters:params];
    [action setRequestMethod:GET];
    
    return action;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    return @{};
}

@end
