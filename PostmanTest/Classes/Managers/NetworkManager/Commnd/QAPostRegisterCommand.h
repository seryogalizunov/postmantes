//
//  QAPostRegisterCommand.h
//  QuizApp
//
//  Created by Lizunov on 10/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
@class QAPostRegisterQuery;

@interface QAPostRegisterCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostRegisterQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
