//
//  QAGetCaterogyWithNameCommand.h
//  QuizApp
//
//  Created by Lizunov on 10/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
@class  QAGetCaterogiesQuery;

@interface QAGetCaterogiesCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetCaterogiesQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
