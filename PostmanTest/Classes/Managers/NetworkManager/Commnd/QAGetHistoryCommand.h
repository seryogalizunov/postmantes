//
//  QAGetHistoryCommand.h
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"

@class QAGetHistoryQuery;

@interface QAGetHistoryCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetHistoryQuery *)query
                  successBlock:(CommandSuccessBlock)success
                  failureBlock:(CommandFailureBlock)failure;

@end
