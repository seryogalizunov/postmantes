//
//  QAGetHistoryCommand.m
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAGetHistoryCommand.h"
#import "QAGetHistoryQuery.h"
#import "QAHistories.h"
#import "QAHistory.h"
#import "QAResult.h"
#import "QAAnswer.h"

#import "QAResultCoreDataModel.h"
#import "QAHistoryCoreDataModel.h"
#import "QAHistoriesCoreDataModel.h"

static NSString *const QAGetHistoryCommandPathString = @"history";

@interface QAGetHistoryCommand ()

@property (nonatomic, strong) QAGetHistoryQuery *query;

@end

@implementation QAGetHistoryCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetHistoryQuery *)query
                  successBlock:(CommandSuccessBlock)success
                  failureBlock:(CommandFailureBlock)failure
{
    self = [super initWithClient:client successBlock:success failureBlock:failure];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = nil;
    
    [action setRequestAction:QAGetHistoryCommandPathString];
    [action setRequestParameters:params];
    [action setRequestMethod:GET];
    
    return action;
}


- (NSDictionary *)parseResponceObject:(id)responceObject
{
    NSMutableArray *historiesArray = [[NSMutableArray alloc] init];
    
    for (NSInteger historiesIndex = 0; historiesIndex < [responceObject count]; historiesIndex++)
    {
        NSMutableArray *resultHistoryArray = [[NSMutableArray alloc] init];
        NSArray *resultArray = [[responceObject objectAtIndex:historiesIndex] objectForKey:@"result"];
        
        for (NSInteger resultHistroyIndex = 0; resultHistroyIndex < [resultArray count]; resultHistroyIndex++)
        {
            NSMutableArray *resultAnswersArray = [[NSMutableArray alloc] init];
            NSArray *answersArray = [[resultArray objectAtIndex:resultHistroyIndex] objectForKey:@"answers"];
            
            for (NSInteger answerIndex = 0; answerIndex < [answersArray count]; answerIndex++)
            {
                QAAnswer *answerModel = [[QAAnswer alloc] initWithAnswer:[answersArray objectAtIndex:answerIndex]];
                [resultAnswersArray addObject:answerModel];
            }
            QAResult *resultModel = [[QAResult alloc] initWithAnswersArray:resultAnswersArray
                                                                  question:[[resultArray objectAtIndex:resultHistroyIndex] objectForKey:@"question"]
                                                               rightAnswer:[[resultArray objectAtIndex:resultHistroyIndex] objectForKey:@"rightAnswer"]
                                                                userAnswer:[[resultArray objectAtIndex:resultHistroyIndex] objectForKey:@"userAnswer"]];
            [resultHistoryArray addObject:resultModel];
        }
        QAHistory *historyModel = [[QAHistory alloc] initWithResultsModel:resultHistoryArray
                                                             categoryName:[[responceObject objectAtIndex:historiesIndex] objectForKey:@"name"]
                                                                     date:[self convertTimeIntervalToDate:[[[responceObject objectAtIndex:historiesIndex] objectForKey:@"date"] doubleValue]]];
        [historiesArray addObject:historyModel];
    }
    QAHistories *historiesModel = [[QAHistories alloc] initWithAccessToken:@""
                                                              historyArray:historiesArray];
    
    return @{NSStringFromClass([QAHistories class]): historiesModel};
}

- (NSDate *)convertTimeIntervalToDate:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    return date;
}

@end
