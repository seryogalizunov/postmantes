//
//  QAPostLogoutCommand.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
@class QAPostLogoutQuery;

@interface QAPostLogoutCommand : QABaseCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostLogoutQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock;

@end
