//
//  QAGetChampionshipCommand.m
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAGetChampionshipCommand.h"
#import "QAGetChampionshipQuery.h"
#import "QAChampionship.h"
#import "QAQuestion.h"
#import "QAAnswer.h"


static NSString *const QAGetChampionshipCommandPathString = @"championship";

@interface QAGetChampionshipCommand ()

@property (nonatomic, strong) QAGetChampionshipQuery* query;

@end

@implementation QAGetChampionshipCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAGetChampionshipQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = nil;
    
    [action setRequestAction:[self URLstringWithPath:QAGetChampionshipCommandPathString
                                              offset:[[self query] offset]]];
    [action setRequestParameters:params];
    [action setRequestMethod:GET];
    
    return action;
}

- (NSString *)URLstringWithPath:(NSString *)path
                         offset:(NSString *)offset
{
    NSString *resultString = [NSString stringWithFormat:@"%@?offset=%@", path, offset];
    return resultString;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    NSMutableArray *categoriesArray = [[NSMutableArray alloc] init];
    
    for (NSInteger indexQuestions = 0; indexQuestions < [responceObject count]; indexQuestions++)
    {
        NSMutableArray *resultAnswerArray = [[NSMutableArray alloc] init];
        NSArray *answersArray = [[responceObject objectAtIndex:indexQuestions] objectForKey:@"answers"];
        
        for (NSInteger indexAnswer = 0; indexAnswer < [answersArray count]; indexAnswer++)
        {
            QAAnswer *answer = [[QAAnswer alloc] initWithAnswer:[answersArray objectAtIndex:indexAnswer]];            
            [resultAnswerArray addObject:answer];
        }
        QAQuestion *questionModel = [[QAQuestion alloc] initWithAnswersArray:resultAnswerArray
                                                                  categoryId:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"categoryId"]
                                                                    objectId:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"objectId"]
                                                                    question:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"question"]
                                                                 rightAnswer:[[responceObject objectAtIndex:indexQuestions] objectForKey:@"rightAnswer"]];
        [categoriesArray addObject:questionModel];
    }
    QAChampionship *championshipModel = [[QAChampionship alloc] initWithQuestionsArray:categoriesArray];    
    return @{NSStringFromClass([QAChampionship class]): championshipModel};
}

@end
