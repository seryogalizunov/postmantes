//
//  QAPostLoginCommand.m
//  QuizApp
//
//  Created by Lizunov on 10/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAPostLoginCommand.h"
#import "QAToken.h"
#import "QAPostLoginQuery.h"

static NSString *const QAPostLoginCommandPathString = @"login";


@interface QAPostLoginCommand ()

@property (nonatomic, strong) QAPostLoginQuery *query;

@end

@implementation QAPostLoginCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostLoginQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[[self query] email], @"email", [[self query] password], @"password", nil];
    
    [action setRequestAction:QAPostLoginCommandPathString];
    [action setRequestParameters:params];
    [action setRequestMethod:POST];
    
    return action;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    QAToken *tokenModel = [[QAToken alloc] initWithToken:[responceObject objectForKey:@"token"]];
    return @{NSStringFromClass([QAToken class]): tokenModel};
}

@end
