//
//  QAPostRegisterCommand.m
//  QuizApp
//
//  Created by Lizunov on 10/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAPostRegisterCommand.h"
#import "QARegister.h"
#import "QAPostRegisterQuery.h"

static NSString *const QAPostRegisteCommandPathString = @"register";

@interface QAPostRegisterCommand ()

@property (nonatomic, strong) QAPostRegisterQuery *query;

@end

@implementation QAPostRegisterCommand

- (instancetype)initWithClient:(QANetworkClient *)client
                         query:(QAPostRegisterQuery *)query
                  successBlock:(CommandSuccessBlock)successBlock
                  failureBlock:(CommandFailureBlock)failureBlock
{
    self = [super initWithClient:client successBlock:successBlock failureBlock:failureBlock];
    if (self)
    {
        [self setQuery:query];
    }
    return self;
}

#pragma mark -
#pragma mark - Private method

- (QARequestAction *)createAction
{
    QARequestAction *action = [[QARequestAction alloc] init];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[[self query] email], @"email", [[self query] password], @"password", nil];
    
    [action setRequestAction:QAPostRegisteCommandPathString];
    [action setRequestParameters:params];
    [action setRequestMethod:POST];
    
    return action;
}

- (NSDictionary *)parseResponceObject:(id)responceObject
{
    QARegister *registerModel = [[QARegister alloc] initWithEmaeil:[responceObject objectForKey:@"email"]
                                                             login:[responceObject objectForKey:@"login"]];
    
    return @{NSStringFromClass([QARegister class]): registerModel};
}

@end
