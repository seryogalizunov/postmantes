//
//  QANetworkClient.h
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QANetworkConfiguration.h"
#import "QARequestAction.h"
#import "QAUserDefaultsManager.h"
#import "QAError.h"

typedef void (^NetworkClientSuccessBlock)(id responseObject);
typedef void (^NetworkClientFailureBlock)(QAError *theError);

@interface QANetworkClient: NSObject

- (instancetype)initWithConfiguration:(QANetworkConfiguration *)configuration
                  userDefaultsManager:(QAUserDefaultsManager *)userDefailts;

- (void)sendRequestWithAction:(QARequestAction *)requestAction
                 successBlock:(NetworkClientSuccessBlock)successBlock
                 failureBlock:(NetworkClientFailureBlock)failureBlock;

@end
