//
//  QANetworkClient.m
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QANetworkClient.h"
#import <AFNetworking/AFNetworking.h>
#import "QAUserDefaultsManager.h"
#import "QAError.h"

@interface QANetworkClient ()

@property (nonatomic, strong) QANetworkConfiguration *configuration;
@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic, strong) AFHTTPSessionManager *managerWithToken;
@property (nonatomic, strong) QAUserDefaultsManager *userDefaults;
@property (nonatomic, strong) QAError *serverError;

@end

@implementation QANetworkClient

#pragma mark - 
#pragma mark - Init Cofiguration

- (instancetype)initWithConfiguration:(QANetworkConfiguration *)configuration
                  userDefaultsManager:(QAUserDefaultsManager *)userDefailts
{
    self = [super init];
    
    if (self)
    {
        [self setConfiguration:configuration];
        [self setUserDefaults:userDefailts];
        [self setManager:[self configureManagerWithActionMethod]];
        
        [self setManagerWithToken:[self configureManagerWithActionMethod]];
        [self createManagetWithToken];
        [self setServerError:[QAError errorWithDomain:@""
                                                 code:0
                                            userInfo:nil]];
    }
    
    return self;
}

- (AFHTTPSessionManager *)configureManagerWithActionMethod
{
    [self setManager:[AFHTTPSessionManager manager]];
        
    [[[self manager] requestSerializer] setValue:[[self configuration] baseContentTypeValue]
                              forHTTPHeaderField:[[self configuration] baseContentType]];
    return [self manager];
}

#pragma mark - 
#pragma mark - Send request with action

- (void)sendRequestWithAction:(QARequestAction *)requestAction
                 successBlock:(NetworkClientSuccessBlock)successBlock
                 failureBlock:(NetworkClientFailureBlock)failureBlock
{
    switch ([requestAction requestMethod])
    {
        case GET:
            [self makeGETRequestWithRequestAction:requestAction
                                     successBlock:successBlock
                                     failureBlock:failureBlock];
            break;
        case POST:
            [self makePOSTRequestWithRequestAction:requestAction
                                      successBlock:successBlock
                                      failureBlock:failureBlock];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - Make request method

- (void)makeGETRequestWithRequestAction:(QARequestAction *)requestAction
                           successBlock:(NetworkClientSuccessBlock)successBlock
                           failureBlock:(NetworkClientFailureBlock)failureBlock
{
    [self getManagerWithGetRequest:requestAction];
    [[self managerWithToken] GET:[self createURLForRequest:requestAction]
                      parameters:[requestAction requestParameters]
                        progress:nil
                         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                        {
                            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                               options:NSJSONReadingMutableContainers
                                                                                                 error:nil];
                            successBlock(responseDictionary);
                        }
                         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                        {
                            __weak typeof(self) weakSelf = self;
                            failureBlock([[weakSelf serverError] parsingJSONError:error]);
                        }];
}

- (void)makePOSTRequestWithRequestAction:(QARequestAction *)requestAction
                            successBlock:(NetworkClientSuccessBlock)successBlock
                            failureBlock:(NetworkClientFailureBlock)failureBlock
{
    [[self postRequestManagerWithRequestAction:requestAction] POST:[self createURLForRequest:requestAction]
                                                        parameters:[requestAction requestParameters]
                                                          progress:nil
                                                           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                                                            {
                                                                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                                                                   options:NSJSONReadingMutableContainers
                                                                                                                                     error:nil];
                                                                successBlock(responseDictionary);
                                                            }
                                                           failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                                                            {
                                                                __weak typeof(self) weakSelf = self;
                                                                failureBlock([[weakSelf serverError] parsingJSONError:error]);
                                                            }];
}

#pragma mark -
#pragma mark - Create string with URL for request

- (NSString *)createURLForRequest:(QARequestAction *)request
{
    return [NSString stringWithFormat:@"%@%@", [[self configuration] baseURLString], [request requestAction]];
}

#pragma mark - 
#pragma mark - POST Request choice Manager with request action

- (AFHTTPSessionManager *)postRequestManagerWithRequestAction:(QARequestAction *)requestAction
{
    if ([[requestAction requestAction] isEqualToString:@"logout"] || [[requestAction requestAction] isEqualToString:@"history"])
    {
        return [self managerWithToken];
    }
    else
    {
        return [self manager];
    }
}

- (void)getManagerWithGetRequest:(QARequestAction *)requestAction
{
    if ([[requestAction requestAction] isEqualToString:@"category"])
    {
        [self createManagetWithToken];
    }
    else
    {
        [self managerWithToken];
    }
}

#pragma mark - 
#pragma mark - Create Manager With Token

- (void)createManagetWithToken
{
    [[self managerWithToken] setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    [[[self managerWithToken] requestSerializer] setValue:@"application/json"
                                       forHTTPHeaderField:@"Content-Type"];
    
    [[[self managerWithToken] requestSerializer] setValue:[[self userDefaults] getToken]
                                       forHTTPHeaderField:@"token"];
    
    [[self managerWithToken] setResponseSerializer:[AFHTTPResponseSerializer serializer]];
}

@end
