//
//  QAPostHistory.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseQuery.h"

@interface QAPostHistoryQuery : QABaseQuery

@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, assign) NSTimeInterval dateInTimeInterval;
@property (nonatomic, copy) NSArray *questionsArray;

@end
