//
//  QAPostRegister.h
//  QuizApp
//
//  Created by Lizunov on 10/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseQuery.h"

@interface QAPostRegisterQuery : QABaseQuery

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;

@end
