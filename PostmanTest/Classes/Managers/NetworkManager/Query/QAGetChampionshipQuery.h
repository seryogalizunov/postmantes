//
//  QAGetChampionship.h
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseQuery.h"

@interface QAGetChampionshipQuery : QABaseQuery

@property (nonatomic, copy) NSString *offset;

@end
