//
//  QAGetCategoryWithQuestions.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseQuery.h"

@interface QAGetCategoryWithQuestionsQuery : QABaseQuery

@property (nonatomic, copy) NSString *where;
@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *pageSize;

@end
