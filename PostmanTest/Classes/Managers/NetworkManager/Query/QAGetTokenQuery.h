//
//  QAGetToken.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseQuery.h"

@interface QAGetTokenQuery : QABaseQuery

@property (nonatomic, copy) NSString *token;

@end
