//
//  QANetworkConfiguration.m
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QANetworkConfiguration.h"

static NSString *const QANetworkConfigurationBaseURLString = @"http://127.0.0.1:9999/";
static NSString *const QANetworkConfigurationHeaderString = @"Content-Type";
static NSString *const QANetworkConfigurationHeaderValueString = @"application/json";

@interface QANetworkConfiguration ()

@property (nonatomic, copy, readwrite) NSString *baseURLString;
@property (nonatomic, copy, readwrite) NSString *baseContentType;
@property (nonatomic, copy, readwrite) NSString *baseContentTypeValue;

@end

@implementation QANetworkConfiguration

#pragma mark -
#pragma mark - Public method

+ (instancetype)currentConfiguration
{
    return [QANetworkConfiguration developmentConfiguration];
}

#pragma mark -
#pragma mark - Private method

+ (QANetworkConfiguration *)developmentConfiguration
{
    return [QANetworkConfiguration defaultConfigurationWithURLString:QANetworkConfigurationBaseURLString
                                                         contentType:QANetworkConfigurationHeaderString
                                                    contentTypeValue:QANetworkConfigurationHeaderValueString];
}

+ (QANetworkConfiguration *)defaultConfigurationWithURLString:(NSString *)theURLString
                                                  contentType:(NSString *)theContentType
                                             contentTypeValue:(NSString *)theContentTypeValue
{
    QANetworkConfiguration *configuration = [[QANetworkConfiguration alloc] init];
    
    [configuration setBaseURLString:theURLString];
    [configuration setBaseContentType:theContentType];
    [configuration setBaseContentTypeValue:theContentTypeValue];
    
    return configuration;
}

@end
