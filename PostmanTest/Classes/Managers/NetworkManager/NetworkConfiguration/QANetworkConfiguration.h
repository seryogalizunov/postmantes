//
//  QANetworkConfiguration.h
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface QANetworkConfiguration: NSObject

@property (nonatomic, copy, readonly) NSString *baseURLString;
@property (nonatomic, copy, readonly) NSString *baseContentType;
@property (nonatomic, copy, readonly) NSString *baseContentTypeValue;

+ (instancetype)currentConfiguration;

@end
