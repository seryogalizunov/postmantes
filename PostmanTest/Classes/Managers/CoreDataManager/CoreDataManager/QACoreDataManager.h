//
//  QACoreDataManager.h
//  QuizApp
//
//  Created by Lizunov on 10/19/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseDataBaseManager.h"
#import "QACoreDataProtocol.h"

@interface QACoreDataManager : QABaseDataBaseManager <QACoreDataProtocol>

@end
