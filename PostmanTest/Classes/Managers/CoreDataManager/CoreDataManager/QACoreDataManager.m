//
//  QACoreDataManager.m
//  QuizApp
//
//  Created by Lizunov on 10/19/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QACoreDataManager.h"

#import "QAHistoriesCoreDataModel.h"
#import "QAHistoryCoreDataModel.h"
#import "QAResultCoreDataModel.h"
#import "QAHistories.h"
#import "QAHistory.h"
#import "NSDate+NSDateCategory.h"

@implementation QACoreDataManager

#pragma mark -
#pragma mark - QAHistoriesCoreDataModel

- (QAHistoriesCoreDataModel *)newHistories
{
    QAHistoriesCoreDataModel *histories = [self newObjectForEntityName:@"QAHistoriesCoreDataModel" context:[self getMainContext]];
    return histories;
}

- (QAHistoriesCoreDataModel *)getHistoriesWithToken:(NSString *)token
{
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"QAHistoriesCoreDataModel"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"accessToken == %@", token];
    [request setPredicate:predicate];
    
    NSArray* result = [[self getMainContext] executeFetchRequest:request error:nil];
    
    QAHistoriesCoreDataModel *historiesModel = [result firstObject];
    if ([result count] > 0)
    {
        return historiesModel;
    }
    return nil;
}

#pragma mark -
#pragma mark - QAHistoryCoreDataModel

- (QAHistoryCoreDataModel *)newHistory
{
    QAHistoryCoreDataModel *history = [self newObjectForEntityName:@"QAHistoryCoreDataModel" context:[self getMainContext]];
    return history;
}

#pragma mark -
#pragma mark - QAResultCoreDataModel

- (QAResultCoreDataModel *)newResult
{
    QAResultCoreDataModel *result = [self newObjectForEntityName:@"QAResultCoreDataModel" context:[self getMainContext]];
    return result;
}

#pragma mark -
#pragma mark - Delete HistoryCoreDataModel

- (void)deleteHistoryCoreDataModel:(QAHistoriesCoreDataModel *)historyCoreDataNodel
{
    NSManagedObjectContext *context = [self getMainContext];
    if (historyCoreDataNodel != nil)
    {
        [context deleteObject:historyCoreDataNodel];
    }
}

#pragma mark -
#pragma mark - Save History To CoreDate

- (void)saveHistoriesInCoreDataModel:(id)networkModel
                      andAccessToken:(NSString *)accessToken
{
    QAHistoriesCoreDataModel *historiesResult = [self newHistories];
    
    QAHistories *historiesTemp = networkModel[NSStringFromClass([QAHistories class])];
    NSMutableSet *historiesSet = [[NSMutableSet alloc] init];
    
    for (NSInteger indexHistories = 0; indexHistories < [[historiesTemp histories] count]; indexHistories++)
    {
        QAHistoryCoreDataModel *historyModel = [self newHistory];
        [historyModel setCategoryName:[[[historiesTemp histories] objectAtIndex:indexHistories] categoryName]];
        [historyModel setDate:[[[historiesTemp histories] objectAtIndex:indexHistories] date]];
        
        NSArray<QAResult *> *resultArray = [NSArray arrayWithArray:[[[historiesTemp histories] objectAtIndex:indexHistories] results]];
        NSMutableSet *resultMutableSet = [[NSMutableSet alloc] init];
        
        for (NSInteger indexResult = 0; indexResult < [resultArray count]; indexResult++)
        {
            NSMutableArray *answerResultMutableArray = [[NSMutableArray alloc] init];
            QAResultCoreDataModel *resultModel = [self newResult];
            [resultModel setQuestion:[[resultArray objectAtIndex:indexResult] question]];
            [resultModel setRightAnswer:[[resultArray objectAtIndex:indexResult] rightAnswer]];
            [resultModel setUserAnswer:[[resultArray  objectAtIndex:indexResult] userAnswer]];
            
            NSArray *answerArray = [[resultArray objectAtIndex:indexResult] answers];
            
            for (NSInteger indexAnswer = 0; indexAnswer < [answerArray count]; indexAnswer++)
            {
                [answerResultMutableArray addObject:[[answerArray objectAtIndex:indexAnswer] answer]];
            }
            NSData *answerData = [NSKeyedArchiver archivedDataWithRootObject:answerResultMutableArray];
            
            [resultModel setAnswers:answerData];
            [resultMutableSet addObject:resultModel];
        }
        [historyModel setResults:resultMutableSet];
        [historiesSet addObject:historyModel];
    }
    
    [historiesResult setHistories:historiesSet];
    [historiesResult setAccessToken:accessToken];
    
    [self saveContextAsync:[self getMainContext]];
}

#pragma mark -
#pragma mark - Сonvert QAHistoriesCoreDataModel in QAHistories

- (NSDictionary *)convertHistoriesCoreDataModelInHistoriesNetworkModel:(QAHistoriesCoreDataModel *)historiesCoreDataModel
{
    NSMutableArray *historiesArray = [[NSMutableArray alloc] init];
    NSArray<QAHistoryCoreDataModel *> *historiesModel = [[historiesCoreDataModel histories] allObjects];
    
    for (NSInteger indexHistoies = 0; indexHistoies < [historiesModel count]; indexHistoies++)
    {
        NSMutableArray *resultHistoryArray = [[NSMutableArray alloc] init];
        NSArray<QAResultCoreDataModel *> *resultArray = [[[historiesModel objectAtIndex:indexHistoies] results] allObjects];
        
        for (NSInteger indexResult = 0; indexResult < [resultArray count]; indexResult++)
        {
            NSMutableArray *answerResultMutableArray = [[NSMutableArray alloc] init];
            NSArray *answerArray = [NSArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:[[resultArray objectAtIndex:indexResult] answers]]];
            
            for (NSInteger indexAnswer = 0; indexAnswer < [answerArray count]; indexAnswer++)
            {
                QAAnswer *answer = [[QAAnswer alloc] initWithAnswer:[answerArray objectAtIndex:indexAnswer]];
                [answerResultMutableArray addObject:answer];
            }
            QAResult *result = [[QAResult alloc] initWithAnswersArray:answerResultMutableArray
                                                             question:[[resultArray objectAtIndex:indexResult] question]
                                                          rightAnswer:[[resultArray objectAtIndex:indexResult] rightAnswer]
                                                           userAnswer:[[resultArray objectAtIndex:indexResult] userAnswer]];
            [resultHistoryArray addObject:result];
        }
        QAHistory *history= [[QAHistory alloc] initWithResultsModel:resultHistoryArray
                                                       categoryName:[[historiesModel objectAtIndex:indexHistoies] categoryName]
                                                               date:[[historiesModel objectAtIndex:indexHistoies] date]];
        [historiesArray addObject:history];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                   ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [historiesArray sortedArrayUsingDescriptors:sortDescriptors];
    
    QAHistories *histories= [[QAHistories alloc] initWithAccessToken:[historiesCoreDataModel accessToken]
                                                        historyArray:sortedArray];
    
    return @{NSStringFromClass([QAHistories class]): histories};
}

@end
