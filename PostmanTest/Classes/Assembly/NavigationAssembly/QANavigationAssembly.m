//
//  QANavigationAssembly.m
//  QuizApp
//
//  Created by Lizunov on 11/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QANavigationAssembly.h"
#import "QAMainViewController.h"
#import "QACaterogyQuestionsPageViewController.h"
#import "QALoginVIewController.h"
#import "QATestViewController.h"
#import "QAHistoryViewController.h"
#import "QAResultViewController.h"
#import "QAChampionshipPageViewController.h"
#import "QADetailedResultViewController.h"
#import "QASingUpViewController.h"

#import "QACategory.h"
#import "QAHistory.h"
#import "QAResult.h"

@interface QANavigationAssembly ()

@property (nonatomic, strong) UINavigationController *navigationController;

@end

@implementation QANavigationAssembly

#pragma mark -
#pragma mark - QAAssemblyProtocol

#pragma mark -
#pragma mark - Create MainViewController

- (QAMainViewController *)createMainViewController
{
    QAMainViewController *mainViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QAMainViewController class])];
    return mainViewController;
}

#pragma mark -
#pragma mark - Create LoginViewController

- (QALoginViewController *)createLoginController
{
    QALoginViewController *loginViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QALoginViewController class])];
    return loginViewController;
}

#pragma mark -
#pragma mark - Create SignUpViewController

- (QASingUpViewController *)createSignUpController
{
    QASingUpViewController *loginViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QASingUpViewController class])];
    return loginViewController;
}

#pragma mark -
#pragma mark - Create TestPage

- (QACaterogyQuestionsPageViewController *)createCaterogyQuestionsPageViewController
{
    QACaterogyQuestionsPageViewController *testPageViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QACaterogyQuestionsPageViewController class])];
    return testPageViewController;
}

#pragma mark -
#pragma mark - Create DetailedResultViewControlle

- (QADetailedResultViewController *)createDetailedResultViewController
{
    QADetailedResultViewController *detailedResultViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QADetailedResultViewController class])];
    return detailedResultViewController;
}

#pragma mark -
#pragma mark - Create HistoryViewController

- (QAHistoryViewController *)createHistoryViewController
{
    QAHistoryViewController *historyViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QAHistoryViewController class])];
    return historyViewController;
}

#pragma mark -
#pragma mark - Create ResultViewController

- (QAResultViewController *)createResultViewController
{
    QAResultViewController *resultViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QAResultViewController class])];
    return resultViewController;
}

#pragma mark -
#pragma mark - Create ChampionshipViewController

- (QAChampionshipPageViewController *)createChampionshipViewController
{
    QAChampionshipPageViewController *championshipPageViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([QAChampionshipPageViewController class])];
    return championshipPageViewController;
}

#pragma mark -
#pragma mark - QAMethodsNavigationProtocol

#pragma mark -
#pragma mark - PresentViewController

- (void)presentViewController:(UIViewController *)viewController
{
    [[self navigationController] presentViewController:viewController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - PushViewController

- (void)pushViewController:(UIViewController *)viewController
{
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)pushViewControllerWithTransitionFromLeftAnimation:(UIViewController *)viewController
{
    CATransition* transition = [CATransition animation];
    [transition setDuration:0.25];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:viewController animated:NO];
}

#pragma mark - 
#pragma mark - PopViewController

- (void)popViewController
{
    [[self navigationController] popViewControllerAnimated:YES];    
}

- (void)popViewControllerWithTransitionFromRightAnimation
{
    CATransition* transition = [CATransition animation];
    [transition setDuration:0.25];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromRight];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark - PopToRootViewController

- (void)popToRootViewController
{
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

- (void)popToRootViewControllerWithTransitionFromRightAnimation
{
    CATransition* transition = [CATransition animation];
    [transition setDuration:0.25];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromRight];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [[self navigationController] popToRootViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark - DismissViewController

- (void)dismissViewcontroller:(void (^)(void))block
{
    [[self navigationController] dismissViewControllerAnimated:YES completion:block];
}

#pragma mark -
#pragma mark - PerformSetNavigationViewController

- (void)performSetNavigationController:(UINavigationController *)navigationController
{
    [self setNavigationController:navigationController];
}

#pragma mark -
#pragma mark - Private Methods

- (__kindof UIViewController *)assemblyViewControllerWithWithIdentifier:(NSString *)identifier
{
    return [[self assemblyStoryboard] instantiateViewControllerWithIdentifier:identifier];
}

- (UIStoryboard *)assemblyStoryboard
{
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

@end
