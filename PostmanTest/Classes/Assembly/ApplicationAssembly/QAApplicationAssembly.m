//
//  QAApplicationAssembly.m
//  QuizApp
//
//  Created by Lizunov on 11/2/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAApplicationAssembly.h"
#import "QANavigationAssembly.h"
#import "QAComponenttAssembly.h"
#import "QAMainViewController.h"
#import "QACaterogyQuestionsPageViewController.h"
#import "QALoginVIewController.h"
#import "QATestViewController.h"
#import "QAHistoryViewController.h"
#import "QAResultViewController.h"
#import "QAChampionshipPageViewController.h"
#import "QADetailedResultViewController.h"
#import "QANavigationController.h"
#import "QASingUpViewController.h"

@interface QAApplicationAssembly ()

@property (nonatomic, strong) QANavigationAssembly *navigationAssembly;
@property (nonatomic, strong) QAComponenttAssembly *componentAssembly;
@property (nonatomic, strong) QARouter *router;

@property (nonatomic, strong) QANavigationController *navigationController;

@end

@implementation QAApplicationAssembly

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setNavigationAssembly:[[QANavigationAssembly alloc] init]];
        [self setComponentAssembly:[[QAComponenttAssembly alloc] init]];
        [self setRouter:[[QARouter alloc] initWithNAvigationControler:[self navigationAssembly]]];
        [[self router] setAssembly:self];
    }
    return self;
}

#pragma mark - 
#pragma mark - Create LoginViewController

- (UIViewController *)initialViewController
{
    [self setNavigationController:[[QANavigationController alloc] initWithRootViewController:[[self router] initialViewController]]];
    [[self navigationAssembly] performSetNavigationController:[self navigationController]];
    return [self navigationController];
}

- (QALoginViewController *)assemblyLoginViewControllr
{
    QALoginViewController *loginViewController = [[self navigationAssembly] createLoginController];
    [loginViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [loginViewController setRouter:[self router]];
    [loginViewController setAssembly:[[self router] assembly]];
    return loginViewController;
}

#pragma mark -
#pragma mark - Create MainViewController

- (QAMainViewController *)assemblyMainViewController
{
    QAMainViewController *mainViewController = [[self navigationAssembly] createMainViewController];
    [mainViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [mainViewController setRouter:[self router]];
    [mainViewController setAssembly:[[self router] assembly]];
    return mainViewController;
}

#pragma mark -
#pragma mark - Create SignUpViewController

- (QASingUpViewController *)assemblySignUpController
{
    QASingUpViewController *signUpViewController = [[self navigationAssembly] createSignUpController];
    [signUpViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [signUpViewController setRouter:[self router]];
    return signUpViewController;
}

#pragma mark -
#pragma mark - Create TestPage

- (QACaterogyQuestionsPageViewController *)assemblyCaterogyQuestionsPageViewController
{
    QACaterogyQuestionsPageViewController *caterogyQuestionsPageViewController = [[self navigationAssembly] createCaterogyQuestionsPageViewController];
    [caterogyQuestionsPageViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [caterogyQuestionsPageViewController setRouter:[self router]];
    return caterogyQuestionsPageViewController;
}

#pragma mark -
#pragma mark - Create DetailedResultViewControlle

- (QADetailedResultViewController *)assemblyDetailedResultViewController
{
    QADetailedResultViewController *detailedResultViewController = [[self navigationAssembly] createDetailedResultViewController];
    [detailedResultViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [detailedResultViewController setRouter:[self router]];
    return detailedResultViewController;
}

#pragma mark -
#pragma mark - Create HistoryViewController

- (QAHistoryViewController *)assemblyHistoryViewController
{
    QAHistoryViewController *historyViewController = [[self navigationAssembly] createHistoryViewController];
    [historyViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [historyViewController setRouter:[self router]];
    return historyViewController;
}

#pragma mark -
#pragma mark - Create ResultViewController

- (QAResultViewController *)assemblyResultViewController
{
    QAResultViewController *resultViewController = [[self navigationAssembly] createResultViewController];
    [resultViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [resultViewController setRouter:[self router]];
    return resultViewController;
}

#pragma mark -
#pragma mark - Create ChampionshipViewController

- (QAChampionshipPageViewController *)assemblyChampionshipViewController
{
    QAChampionshipPageViewController *championshipPageViewController = [[self navigationAssembly] createChampionshipViewController];
    [championshipPageViewController setDataManager:[[self componentAssembly] assemblyDataManager]];
    [championshipPageViewController setRouter:[self router]];
    return championshipPageViewController;
}

@end
