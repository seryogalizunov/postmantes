//
//  QAApplicationAssembly.h
//  QuizApp
//
//  Created by Lizunov on 11/2/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QAAssemblyProtocol.h"

@interface QAApplicationAssembly : NSObject <QAAssemblyProtocol>

@end
