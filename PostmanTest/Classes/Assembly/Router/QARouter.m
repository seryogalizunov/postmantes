//
//  QARouter.m
//  QuizApp
//
//  Created by Lizunov on 10/18/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QARouter.h"
#import "QAApplicationAssembly.h"
#import "QAServerSideManager.h"
#import "QAHistoryCoreDataModel.h"
#import "QAMainViewController.h"
#import "QACaterogyQuestionsPageViewController.h"
#import "QALoginVIewController.h"
#import "QATestViewController.h"
#import "QAHistoryViewController.h"
#import "QAResultViewController.h"
#import "QAChampionshipPageViewController.h"
#import "QADetailedResultViewController.h"
#import "QAMethodsNavigationProtocol.h"
#import "QANavigationAssembly.h"
#import "QAApplicationAssembly.h"
#import "QANavigationController.h"
#import "QASingUpViewController.h"

@interface QARouter ()

@property (nonatomic, strong) QAServerSideManager *serverManager;
@property (nonatomic, strong) id<QAMethodsNavigationProtocol> navigationProtocol;

@end

@implementation QARouter

- (instancetype)initWithNAvigationControler:(QANavigationAssembly *)navigationController
{
    self = [super init];
    if (self)
    {
        [self setNavigationProtocol:navigationController];
    }
    return self;
}

- (QAMainViewController *)initialViewController
{
    return [[self assembly] assemblyMainViewController];
}

#pragma mark -
#pragma mark - Show LoginViewController

- (void)showLoginViewController
{
    QALoginViewController *loginViewController = [[self assembly] assemblyLoginViewControllr];
    [[self navigationProtocol] pushViewControllerWithTransitionFromLeftAnimation:loginViewController];
}

#pragma mark -
#pragma mark - Show SignUpViewController

- (void)showSignUpController
{
    QASingUpViewController *loginViewController = [[self assembly] assemblySignUpController];
    [[self navigationProtocol] pushViewController:loginViewController];
}

#pragma mark -
#pragma mark - Show QAMainViewController

- (void)showMainViewController
{
    QAMainViewController *mainViewController = [[self assembly] assemblyMainViewController];
    [[self navigationProtocol] pushViewController:mainViewController];
}

#pragma mark -
#pragma mark - Show QACaterogyQuestionsPageViewController

- (void)showCaterogyQuestionsPageViewControllerWithCategory:(QACategory *)category
{
    QACaterogyQuestionsPageViewController *categoryQuestionViewController = [[self assembly] assemblyCaterogyQuestionsPageViewController];
    [categoryQuestionViewController setCategory:category];
    [[self navigationProtocol] pushViewController:categoryQuestionViewController];
}

#pragma mark -
#pragma mark - Show QAHistoryViewController

- (void)showHistoryViewController
{
    QAHistoryViewController *historyViewController = [[self assembly] assemblyHistoryViewController];
    [[self navigationProtocol] pushViewController:historyViewController];
}

#pragma mark -
#pragma mark - Show QAResultViewController

- (void)showResultViewControllerWithHistory:(QAHistory *)history
{
    QAResultViewController *resultViewController = [[self assembly] assemblyResultViewController];
    [resultViewController setHistory:history];
    [[self navigationProtocol] pushViewController:resultViewController];
}

#pragma mark -
#pragma mark - Show QADetailedResultViewController

- (void)showDetailedResultViewControllerWithResult:(QAResult *)result
{
    QADetailedResultViewController *detailedResultViewController = [[self assembly] assemblyDetailedResultViewController];
    [detailedResultViewController setResult:result];
    [[self navigationProtocol] pushViewController:detailedResultViewController];
}

#pragma mark -
#pragma mark - Show QAChampionshipPageViewController

- (void)showChampionshipViewController
{
    QAChampionshipPageViewController *championchipPageViewController = [[self assembly] assemblyChampionshipViewController];
    [[self navigationProtocol] pushViewController:championchipPageViewController];
}

#pragma mark - 
#pragma mark - NavigationAssembly Methods

#pragma mark -
#pragma mark - PopViewController

- (void)popViewController
{
    [[self navigationProtocol] popViewController];
}

- (void)popViewControllerWithTransitionFromRightAnimation
{
    [[self navigationProtocol] popViewControllerWithTransitionFromRightAnimation];
}

#pragma mark -
#pragma mark - PopToRootViewController

- (void)popToRootViewController
{
    [[self navigationProtocol] popToRootViewController];
}

- (void)popToRootViewControllerWithTransitionFromRightAnimation
{
    [[self navigationProtocol] popToRootViewControllerWithTransitionFromRightAnimation];
}

#pragma mark - 
#pragma mark - DismissViewController

- (void)dismissViewcontroller:(void (^)(void))block
{
    [[self navigationProtocol] dismissViewcontroller:block];
}

@end
