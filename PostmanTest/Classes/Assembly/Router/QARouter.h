//
//  QARouter.h
//  QuizApp
//
//  Created by Lizunov on 10/18/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QARouterProtocol.h"

@class QAApplicationAssembly;
@class QANavigationAssembly;

@interface QARouter : NSObject <QARouterProtocol>

@property (nonatomic, weak) QAApplicationAssembly *assembly;

- (instancetype)initWithNAvigationControler:(QANavigationAssembly *)navigationController;

@end
