//
//  QAContentAssembly.m
//  QuizApp
//
//  Created by Lizunov on 11/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAComponenttAssembly.h"
#import "QADataManager.h"
#import "QARouter.h"
#import "QAMainViewController.h"
#import "QANetworkConfiguration.h"
#import "QAServerSideManager.h"
#import "QANetworkClient.h"
#import "QACoreDataManager.h"

@implementation QAComponenttAssembly

#pragma mark -
#pragma mark - Aseembly Methods

- (QADataManager *)assemblyDataManager
{
    return [[QADataManager alloc] initWithServerSideManager:[self assemblyServerManager]
                                            coreDataManager:[self assemblyCoreDataManager]
                                         userDefaultManager:[self assemblyUserDefaultsManager]];
}

- (QANetworkConfiguration *)assemblyNetworkConfigurator
{
    return [QANetworkConfiguration currentConfiguration];
}

- (QANetworkClient *)assemblyNetworkClient
{
    return [[QANetworkClient alloc] initWithConfiguration:[self assemblyNetworkConfigurator]
                                      userDefaultsManager:[self assemblyUserDefaultsManager]];
}

- (QAServerSideManager *)assemblyServerManager
{
    return [[QAServerSideManager alloc] initWithNetworkClient:[self assemblyNetworkClient]];
}

- (QACoreDataManager *)assemblyCoreDataManager
{
    return [[QACoreDataManager alloc] init];
}

- (QAUserDefaultsManager *)assemblyUserDefaultsManager
{
    return [[QAUserDefaultsManager alloc] init];
}

@end
