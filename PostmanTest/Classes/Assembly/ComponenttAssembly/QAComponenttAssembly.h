//
//  QAContentAssembly.h
//  QuizApp
//
//  Created by Lizunov on 11/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAComponenttAssemblyProtocol.h"

@interface QAComponenttAssembly : NSObject <QAComponenttAssemblyProtocol>

@end
