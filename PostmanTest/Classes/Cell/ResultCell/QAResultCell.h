//
//  QAResultCell.h
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAResult;

@interface QAResultCell : UICollectionViewCell

- (void)performLabelCellWithResultNetworkModel:(QAResult *)result
                                  indexPathRow:(NSInteger)row;

@end
