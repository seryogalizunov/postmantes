//
//  QAResultCell.m
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAResultCell.h"
#import "QAResultCoreDataModel.h"
#import "QAResult.h"

@interface QAResultCell ()

@property (weak, nonatomic) IBOutlet UILabel *numberResultLable;

@end

@implementation QAResultCell

#pragma mark - 
#pragma mark - performLabelCellWithResult

- (void)performLabelCellWithResultNetworkModel:(QAResult *)result
                                  indexPathRow:(NSInteger)row
{
    NSInteger numberQuestions = row + 1;
    [[self numberResultLable] setText:[NSString stringWithFormat:@"%ld", numberQuestions]];
    [[self numberResultLable] setBackgroundColor:[self rangeOfColorDependingOnTestResultNetworkModel:result
                                                                                        indexPathRow:row]];
    [[[self numberResultLable] layer] setCornerRadius:10.f];
    [[self numberResultLable] setClipsToBounds:YES];
}

#pragma mark -
#pragma mark - Private Methods

- (UIColor *)rangeOfColorDependingOnTestResultNetworkModel:(QAResult *)resultNetworkModel
                                              indexPathRow:(NSInteger)row
{
    NSInteger userAnswer =[[resultNetworkModel userAnswer] integerValue];
    NSInteger rightAnswer = [[resultNetworkModel rightAnswer] integerValue];
    NSInteger countAnswers = [[resultNetworkModel answers] count];
    
    if (userAnswer == rightAnswer)
    {
        return [UIColor greenColor];
    }
    else if (((userAnswer + 1) == rightAnswer) && (rightAnswer == countAnswers))
    {
        return [UIColor greenColor];
    }
    return [UIColor redColor];
}

@end
