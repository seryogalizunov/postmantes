//
//  QAHistoryViewCell.h
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QAHistoryCoreDataModel;

@interface QAHistoryViewCell : UITableViewCell

- (void)performHistoryCellWithCategoryName:(NSString *)nameCategory
                                      date:(NSDate *)date;

@end
