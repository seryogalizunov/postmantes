//
//  QAHistoryViewCell.m
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAHistoryViewCell.h"
#import "QAHistoryCoreDataModel.h"
#import "NSDate+NSDateCategory.h"

@interface QAHistoryViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameCategoryLable;
@property (weak, nonatomic) IBOutlet UILabel *dateLable;

@end

@implementation QAHistoryViewCell

#pragma mark -
#pragma mark - Perform Histry Cell

- (void)performHistoryCellWithCategoryName:(NSString *)nameCategory
                                      date:(NSDate *)date
{
    [[self nameCategoryLable] setText:nameCategory];
    [[self dateLable] setText:[date converToString]];
}

@end
