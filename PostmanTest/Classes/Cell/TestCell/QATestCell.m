//
//  QATestCell.m
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QATestCell.h"

@interface QATestCell ()

@property (weak, nonatomic) IBOutlet UILabel *questionsLable;

@end

@implementation QATestCell

- (void)performLableWithTitle:(NSString *)title
{
    [[self questionsLable] setText:title];
}

@end