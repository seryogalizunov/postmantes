//
//  QACategoriesTestCell.m
//  QuizApp
//
//  Created by Lizunov on 11/2/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QACategoriesTestCell.h"

@interface QACategoriesTestCell ()

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@end

@implementation QACategoriesTestCell

- (void)performCategoryName:(NSString *)categoryName
{
    [[self categoryLabel] setText:categoryName];
}

@end
