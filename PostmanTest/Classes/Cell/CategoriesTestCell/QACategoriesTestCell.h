//
//  QACategoriesTestCell.h
//  QuizApp
//
//  Created by Lizunov on 11/2/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QACategoriesTestCell : UITableViewCell

- (void)performCategoryName:(NSString *)categoryName;

@end
