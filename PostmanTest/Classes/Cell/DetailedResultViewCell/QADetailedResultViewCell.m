//
//  QADetailedResultViewCell.m
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QADetailedResultViewCell.h"

@interface QADetailedResultViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *questionsLable;

@end

@implementation QADetailedResultViewCell

- (void)performLableWithTitle:(NSString *)title
{
    [[self questionsLable] setText:title];
}

@end
