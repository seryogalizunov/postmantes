//
//  QADetailedResultViewCell.h
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QADetailedResultViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;

- (void)performLableWithTitle:(NSString *)title;

@end
