//
//  QAChampionshipCell.m
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAChampionshipCell.h"

@interface QAChampionshipCell ()

@property (weak, nonatomic) IBOutlet UILabel *questionsLable;

@end

@implementation QAChampionshipCell

- (void)performLableWithTitle:(NSString *)title
{
    [[self questionsLable] setText:title];
}

@end
