//
//  QAChampionshipCell.h
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QAChampionshipCell : UITableViewCell

- (void)performLableWithTitle:(NSString *)title;

@end
