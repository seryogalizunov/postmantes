//
//  QAChampionshipTableViewDelegate.h
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAQuestion;

@protocol QAChampionshipDelegate <NSObject>

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary;

@end

@interface QAChampionshipTableViewDelegate : NSObject <UITableViewDelegate>

@property (nonatomic, weak) id<QAChampionshipDelegate> delegate;

- (instancetype)initWithQuestion:(QAQuestion *)question;

@end