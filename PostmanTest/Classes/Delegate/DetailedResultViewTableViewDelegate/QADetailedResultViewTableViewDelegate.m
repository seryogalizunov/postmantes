//
//  QADetailedResultViewTableViewDelegate.m
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QADetailedResultViewTableViewDelegate.h"
#import "QADetailedResultHeaderView.h"
#import "QAQuestion.h"
#import "QAResult.h"

@interface QADetailedResultViewTableViewDelegate ()

@property (nonatomic, strong) QAResult *result;

@end

@implementation QADetailedResultViewTableViewDelegate

- (instancetype)initWithResult:(QAResult *)result
{
    self = [super init];
    if (self)
    {
        [self setResult:result];
    }
    return self;
}

#pragma mark
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    QADetailedResultHeaderView *headerView = [[QADetailedResultHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([tableView frame]), 80)];
    [headerView performLabalWithResult:[self result]];
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
