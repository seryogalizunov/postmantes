//
//  QATestTableViewDelegate.m
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QATestTableViewDelegate.h"
#import "QATestHeaderView.h"
#import "QAResultCoreDataModel.h"
#import "QAQuestion.h"

@interface QATestTableViewDelegate ()

@property (nonatomic, strong) QAQuestion *question;
@property (nonatomic, assign) NSUInteger pageIndex;

@end

@implementation QATestTableViewDelegate

- (instancetype)initWithQuestion:(QAQuestion *)question
{
    self = [super init];
    if (self)
    {
        [self setQuestion:question];
    }
    return self;
}

#pragma mark
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    QATestHeaderView *headerView = [[QATestHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([tableView frame]), 80)];
    [headerView performLabalWithQuestion:[self question]];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self delegate] respondsToSelector:@selector(nextPage:requestsDictionary:)])
    {
        NSDictionary *testDictionary = [NSDictionary dictionaryWithObjectsAndKeys: @([indexPath row]), @"userAnswer", [[self question] objectId], @"questionId", nil];
        
        if ([[self delegate] respondsToSelector:@selector(nextPage:requestsDictionary:)])
        {
            [[self delegate] nextPage:([self pageIndex] + 1)requestsDictionary:testDictionary];
        }
    }
}

@end
