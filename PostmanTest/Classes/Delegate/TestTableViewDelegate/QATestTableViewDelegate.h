//
//  QATestTableViewDelegate.h
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QATestHeaderView;
@class QAQuestion;

@protocol QATestDelegate <NSObject>

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary;

@end

@interface QATestTableViewDelegate : NSObject <UITableViewDelegate>

@property (nonatomic, weak) id<QATestDelegate> delegate;

- (instancetype)initWithQuestion:(QAQuestion *)question;

@end
