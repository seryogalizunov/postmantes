//
//  QAHistoryTableViewDelegate.m
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAHistoryTableViewDelegate.h"
#import "QAHistoryCoreDataModel.h"
#import "QAHistories.h"

@interface QAHistoryTableViewDelegate ()

@property (nonatomic, strong) QAHistories *historiesNetworkModel;

@end

@implementation QAHistoryTableViewDelegate

#pragma mark
#pragma mark - UITableViewDelegate

- (instancetype)initWithHistories:(QAHistories *)histories
{
    self = [super init];
    if (self)
    {
        [self setHistoriesNetworkModel:histories];
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self delegate] respondsToSelector:@selector(performHistory:)])
    {
        QAHistory *historyModel = [[[self historiesNetworkModel] histories] objectAtIndex:[indexPath row]];
        [[self delegate] performHistory:historyModel];
    }
}

@end
