//
//  QAHistoryTableViewDelegate.h
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAHistories;
@class QAHistory;

@protocol QAHistoryDelegate <NSObject>

- (void)performHistory:(QAHistory *)history;

@end

@interface QAHistoryTableViewDelegate :  NSObject <UITableViewDelegate>

@property (nonatomic, weak) id<QAHistoryDelegate> delegate;

- (instancetype)initWithHistories:(QAHistories *)histories;

@end

