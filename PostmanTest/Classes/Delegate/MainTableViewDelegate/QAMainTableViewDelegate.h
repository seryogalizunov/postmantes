//
//  QAMainTableViewDelegate.h
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QACategories;
@class QACategory;
@class QARouter;

@protocol QAMainDelegate <NSObject>

- (void)showAlertWithCategory:(QACategory *)category;

@end

@interface QAMainTableViewDelegate : NSObject <UITableViewDelegate>

@property (nonatomic, weak) id<QAMainDelegate> delegete;

- (instancetype)initWithCategory:(QACategories *)categories;

@end