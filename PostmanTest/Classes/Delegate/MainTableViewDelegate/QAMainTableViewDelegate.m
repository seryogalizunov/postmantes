//
//  QAMainTableViewDelegate.m
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAMainTableViewDelegate.h"
#import "QACategories.h"
#import "QARouter.h"

@interface QAMainTableViewDelegate ()

@property (nonatomic, strong) QACategories *categories;

@end

@implementation QAMainTableViewDelegate

- (instancetype)initWithCategory:(QACategories *)categories
{
    self = [super init];
    if (self)
    {
        [self setCategories:categories];
    }
    return self;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self delegete] respondsToSelector:@selector(showAlertWithCategory:)])
    {
        QACategory *category = [[[self categories] categories] objectAtIndex:[indexPath row]];
        [[self delegete] showAlertWithCategory:category];
    }
}

@end