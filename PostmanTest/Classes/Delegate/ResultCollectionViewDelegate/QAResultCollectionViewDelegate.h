//
//  QAResultCollectionViewDelegate.h
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAHistory;
@class QAResult;

@protocol QAResultDelegete <NSObject>

- (void)performResult:(QAResult *)result;

@end

@interface QAResultCollectionViewDelegate : UIViewController <UICollectionViewDelegate>

@property (nonatomic, weak) id<QAResultDelegete> delegate;

- (instancetype)initWithHistory:(QAHistory *)history;

@end
