//
//  QAResultCollectionViewDelegate.m
//  QuizApp
//
//  Created by Lizunov on 10/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAResultCollectionViewDelegate.h"
#import "QAResultCoreDataModel.h"
#import "QAHistoryCoreDataModel.h"
#import "QAHistory.h"

@interface QAResultCollectionViewDelegate ()

@property (nonatomic, strong) QAHistory *history;

@end

@implementation QAResultCollectionViewDelegate

- (instancetype)initWithHistory:(QAHistory *)history
{
    self = [super init];
    if (self)
    {
        [self setHistory:history];
    }
    return self;
}

#pragma mark -
#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self delegate] respondsToSelector:@selector(performResult:)])
    {
        QAResult *resultModel = [[[self history] results] objectAtIndex:[indexPath row]];
        [[self delegate] performResult:resultModel];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth([collectionView frame]) / 2) - 1, (CGRectGetWidth([collectionView frame]) / 2) - 1);
}

@end
