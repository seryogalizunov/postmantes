//
//  QADetailedResultViewControllerDataSource.m
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QADetailedResultViewControllerDataSource.h"

#import "QADetailedResultHeaderView.h"
#import "QAQuestion.h"
#import "QAResultCoreDataModel.h"
#import "QAResult.h"
#import "QADetailedResultViewCell.h"
#import "QAAnswer.h"

@interface QADetailedResultViewControllerDataSource ()

@property (nonatomic, strong) QAResult *result;

@end

@implementation QADetailedResultViewControllerDataSource

- (instancetype)initWithResult:(QAResult *)result
{
    self = [super init];
    if (self)
    {
        [self setResult:result];
    }
    return self;
}

#pragma mark
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self result] answers] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *testIdentifier = @"QADetailedResultViewCell";
    
    QADetailedResultViewCell *cell = [tableView dequeueReusableCellWithIdentifier:testIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell performLableWithTitle:[[[[self result] answers] objectAtIndex:[indexPath row]] answer]];
    
    [[[cell mainView] layer] setBorderColor:[self getCellquestionsLableLayerBorderColorWithResultNetworkModel:[self result]
                                                                                                 indexPathRow:[indexPath row]]];
    [[[cell mainView] layer] setBorderWidth:5];
    
    return cell;
}

#pragma mark -
#pragma mark - Private Methods

- (NSString *)getCellAnswersLabelWithIndexPahtRow:(NSInteger)row
{
    return [[[[self result] answers] objectAtIndex:row] answer];
}

- (CGColorRef)getCellquestionsLableLayerBorderColorWithResultNetworkModel:(QAResult *)resultNetworkModel
                                                             indexPathRow:(NSInteger)row
{
    NSInteger userAnswer =[[resultNetworkModel userAnswer] integerValue];
    NSInteger rightAnswer = [[resultNetworkModel rightAnswer] integerValue];
    NSInteger countAnswers = [[resultNetworkModel answers] count];
    NSLog(@"rightAnswer - %ld, userAnswer - %ld", (long)rightAnswer, (long)userAnswer);
    
    if ((rightAnswer == row) && (userAnswer == rightAnswer))
    {
        return [[UIColor greenColor] CGColor];
    }
    else if (((userAnswer + 1) == countAnswers) && (row == rightAnswer))
    {
        return [[UIColor greenColor] CGColor];
    }
    else if ((userAnswer != rightAnswer) && (row == rightAnswer))
    {
        return [[UIColor greenColor] CGColor];
    }
    else if ((rightAnswer - 1) == row && (rightAnswer == countAnswers))
    {
        return [[UIColor greenColor] CGColor];
    }
    else if (userAnswer == row)
    {
        return [[UIColor redColor] CGColor];
    }
    
    return [[UIColor clearColor] CGColor];
}

@end