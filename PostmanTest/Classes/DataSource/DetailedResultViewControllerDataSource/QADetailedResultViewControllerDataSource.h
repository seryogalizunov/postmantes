//
//  QADetailedResultViewControllerDataSource.h
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAResult;

@interface QADetailedResultViewControllerDataSource : NSObject <UITableViewDataSource>

- (instancetype)initWithResult:(QAResult *)result;

@end