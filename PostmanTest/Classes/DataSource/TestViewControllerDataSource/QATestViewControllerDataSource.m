//
//  QATestViewControllerDataSource.m
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QATestViewControllerDataSource.h"
#import "QATestCell.h"
#import "QATestHeaderView.h"
#import "QAQuestion.h"
#import "QAResultCoreDataModel.h"

@interface QATestViewControllerDataSource ()

@property (nonatomic, strong) QAQuestion *question;

@end

@implementation QATestViewControllerDataSource


- (instancetype)initWithQuestion:(QAQuestion *)question
{
    self = [super init];
    if (self)
    {
        [self setQuestion:question];
    }
    return self;
}

#pragma mark
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self question] answers] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *testIdentifier = @"QATestCell";
    
    QATestCell *cell = [tableView dequeueReusableCellWithIdentifier:testIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell performLableWithTitle:[[[[self question] answers] objectAtIndex:[indexPath row]] answer]];
    
    return cell;
}

@end
