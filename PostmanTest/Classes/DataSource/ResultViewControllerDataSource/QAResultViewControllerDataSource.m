//
//  QAResultViewControllerDataSource.m
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAResultViewControllerDataSource.h"
#import "QAResultCell.h"
#import "QAHistoryCoreDataModel.h"
#import "QAHistory.h"

@interface QAResultViewControllerDataSource ()

@property (nonatomic, strong) QAHistory *history;

@end

@implementation QAResultViewControllerDataSource

#pragma mark -
#pragma mark - InitWithHistory

- (instancetype)initWithHistoryNetworkModel:(QAHistory *)history
{
    self = [super init];
    if (self)
    {
        [self setHistory:history];
    }
    return self;
}

#pragma mark -
#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[self history] results] count];
}

- (QAResultCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QAResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ResultCell" forIndexPath:indexPath];
    
    QAResult *resultModel = [[[self history] results] objectAtIndex:[indexPath row]];
    [cell performLabelCellWithResultNetworkModel:resultModel
                                    indexPathRow:[indexPath row]];
    
    return cell;
}

@end
