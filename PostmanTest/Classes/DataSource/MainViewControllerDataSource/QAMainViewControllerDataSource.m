//
//  QAMainViewControllerDataSource.m
//  QuizApp
//
//  Created by Lizunov on 10/25/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAMainViewControllerDataSource.h"
#import "QACategory.h"
#import "QACategories.h"
#import "QACategoriesTestCell.h"

@interface QAMainViewControllerDataSource ()

@property (nonatomic, strong) QACategories *categories;

@end

@implementation QAMainViewControllerDataSource

- (instancetype)initWithCategory:(QACategories *)categories
{
    self = [super init];
    if (self)
    {
        [self setCategories:categories];
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self categories] categories] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *categoryIdentifier = @"QACategoriesTestCell";
    QACategoriesTestCell *cell = [tableView dequeueReusableCellWithIdentifier:categoryIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *categoriesName = [[[[self categories] categories] objectAtIndex:[indexPath row]] categoryName];
    [cell performCategoryName:categoriesName];
    
    return cell;
}

@end
