//
//  QAMainViewControllerDataSource.h
//  QuizApp
//
//  Created by Lizunov on 10/25/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QACategories;

@interface QAMainViewControllerDataSource : NSObject <UITableViewDataSource>

- (instancetype)initWithCategory:(QACategories *)categories;

@end
