//
//  QAHistoryViewControllerDataSource.m
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAHistoryViewControllerDataSource.h"
#import "QAHistoryViewCell.h"
#import "QAHistories.h"

@interface QAHistoryViewControllerDataSource ()

@property (nonatomic, strong) QAHistories *historiesNetworkModel;

@end

@implementation QAHistoryViewControllerDataSource

- (instancetype)initWithNetworkHistories:(QAHistories *)histories
{
    self = [super init];
    if (self)
    {
        [self setHistoriesNetworkModel:histories];
    }
    return self;
}

#pragma mark
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self historiesNetworkModel] histories] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *testIdentifier = @"QAHistoryViewCell";
    
    QAHistoryViewCell *cell = [tableView dequeueReusableCellWithIdentifier:testIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    QAHistory *historyModel = [[[self historiesNetworkModel] histories] objectAtIndex:[indexPath row]];
    NSString *categoryName = [historyModel categoryName];
    NSDate *date = [historyModel date];
    
    [cell performHistoryCellWithCategoryName:categoryName
                                        date:date];
    return cell;
}

@end
