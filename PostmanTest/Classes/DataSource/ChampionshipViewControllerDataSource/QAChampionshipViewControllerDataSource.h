//
//  QAChampionshipViewControllerDataSource.h
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QAQuestion;

@interface QAChampionshipViewControllerDataSource : NSObject <UITableViewDataSource>

- (instancetype)initWithQuestion:(QAQuestion *)question;

@end
