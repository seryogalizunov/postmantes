//
//  QAChampionshipViewControllerDataSource.m
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAChampionshipViewControllerDataSource.h"
#import "QAQuestion.h"
#import "QAChampionshipCell.h"

@interface QAChampionshipViewControllerDataSource ()

@property (nonatomic, strong) QAQuestion *question;

@end


@implementation QAChampionshipViewControllerDataSource

- (instancetype)initWithQuestion:(QAQuestion *)question
{
    self = [super init];
    if (self)
    {
        [self setQuestion:question];
    }
    return self;
}

#pragma mark
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self question] answers] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *testIdentifier = @"QAChampionshipCell";
    
    QAChampionshipCell *cell = [tableView dequeueReusableCellWithIdentifier:testIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell performLableWithTitle:[[[[self question] answers] objectAtIndex:indexPath.row] answer]];
    
    return cell;
}

@end
