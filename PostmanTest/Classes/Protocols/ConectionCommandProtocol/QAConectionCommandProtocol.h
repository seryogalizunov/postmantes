//
//  QAConectionCommandProtocol.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QAConectionCommandProtocol <NSObject>

@property (nonatomic, strong) id result;
- (NSString *)parsingJSONError:(NSError *)error;

@end
