//
//  QABaseDataBaseProtocol.h
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QABaseDataBaseProtocol <NSObject>

- (NSManagedObjectContext *)getMainContext;

- (NSManagedObjectContext *)getNewDBContext;

- (void)saveContextSync:(NSManagedObjectContext *)context;

- (void)saveContextAsync:(NSManagedObjectContext *)context;

- (void)saveContextAsync:(NSManagedObjectContext *)context
         completionBlock:(void (^)(void))block;

- (void)deleteObject:(NSManagedObject *)object
             context:(NSManagedObjectContext *)context;

- (id)existingObjectWithObjectID:(NSManagedObjectID *)managedObjectID
                         context:(NSManagedObjectContext *)moc;

- (id)newObjectForEntityName:(NSString *)entityName
                     context:(NSManagedObjectContext *)context;

@end
