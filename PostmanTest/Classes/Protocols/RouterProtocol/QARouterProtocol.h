//
//  QARouterProtocol.h
//  QuizApp
//
//  Created by Lizunov on 10/18/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QACategory.h"
#import "QAHistory.h"

@class QAMainViewController;

@protocol QARouterProtocol <NSObject>

- (instancetype)init;

- (QAMainViewController *)initialViewController;
- (void)showLoginViewController;
- (void)showSignUpController;
- (void)showMainViewController;
- (void)showHistoryViewController;
- (void)showChampionshipViewController;

- (void)showCaterogyQuestionsPageViewControllerWithCategory:(QACategory *)category;
- (void)showResultViewControllerWithHistory:(QAHistory *)history;
- (void)showDetailedResultViewControllerWithResult:(QAResult *)result;

- (void)popViewController;
- (void)popViewControllerWithTransitionFromRightAnimation;
- (void)popToRootViewController;
- (void)popToRootViewControllerWithTransitionFromRightAnimation;
- (void)dismissViewcontroller:(void (^)(void))block;

@end
