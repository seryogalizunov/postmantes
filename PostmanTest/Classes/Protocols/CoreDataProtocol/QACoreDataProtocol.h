//
//  QACoreDataProtocol.h
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@class QAHistoriesCoreDataModel;
@class QAHistoryCoreDataModel;
@class QAResultCoreDataModel;

@protocol QACoreDataProtocol <NSObject>

#pragma mark -
#pragma mark - QAHistoriesCoreDataModel

- (QAHistoriesCoreDataModel *)newHistories;
- (QAHistoriesCoreDataModel *)getHistoriesWithToken:(NSString *)token;

#pragma mark -
#pragma mark - QAHistoryCoreDataModel

- (QAHistoryCoreDataModel *)newHistory;

#pragma mark -
#pragma mark - QAResultCoreDataModel

- (QAResultCoreDataModel *)newResult;

#pragma mark -
#pragma mark - Save History To CoreDate

- (void)saveHistoriesInCoreDataModel:(id)networkModel
                      andAccessToken:(NSString *)accessToken;

#pragma mark -
#pragma mark - Delete HistoryCoreDataModel

- (void)deleteHistoryCoreDataModel:(QAHistoriesCoreDataModel *)historyCoreDataNodel;

#pragma mark -
#pragma mark - Сonvert HistoriesCoreDataModel in HistoriesNetwork

- (NSDictionary *)convertHistoriesCoreDataModelInHistoriesNetworkModel:(QAHistoriesCoreDataModel *)historiesCoreDataModel;

@end
