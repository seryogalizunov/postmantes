//
//  QANavigationAssemblyProtocol.h
//  QuizApp
//
//  Created by Lizunov on 11/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@class QAMainViewController;
@class QALoginViewController;
@class QACaterogyQuestionsPageViewController;
@class QAHistoryViewController;
@class QAResultViewController;
@class QAChampionshipPageViewController;
@class QADetailedResultViewController;
@class QASingUpViewController;

@protocol QANavigationAssemblyProtocol <NSObject>

- (QAMainViewController *)createMainViewController;
- (QALoginViewController *)createLoginController;
- (QASingUpViewController *)createSignUpController;
- (QACaterogyQuestionsPageViewController *)createCaterogyQuestionsPageViewController;
- (QADetailedResultViewController *)createDetailedResultViewController;
- (QAHistoryViewController *)createHistoryViewController;
- (QAResultViewController *)createResultViewController;
- (QAChampionshipPageViewController *)createChampionshipViewController;

@end
