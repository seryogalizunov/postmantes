//
//  QABaseCommand__Protected.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseCommand.h"
#import "QARequestAction.h"
#import "QANetworkClient.h"
#import "QAGetTokenQuery.h"

@interface QABaseCommand ()

@property (nonatomic, strong) QANetworkClient *networkClient;

- (QARequestAction *)createAction;
- (void)performRequestWithAction:(QARequestAction *)action;
- (NSDictionary *)parseResponceObject:(id)responceObject;

@end
