//
//  QALocalStorageProtocol.h
//  QuizApp
//
//  Created by Lizunov on 10/26/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QALocalStorageProtocol <NSObject>

- (void)saveObject:(id)object forKey:(NSString *)key;
- (id)getObjectForKey:(NSString *)key;
- (void)resetObjectforKey:(NSString *)key;

@end
