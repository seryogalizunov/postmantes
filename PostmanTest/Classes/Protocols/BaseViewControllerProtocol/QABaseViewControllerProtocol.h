//
//  QABaseViewControllerProtocol.h
//  QuizApp
//
//  Created by Lizunov on 11/1/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QADataManager.h"
#import "QARouter.h"
#import "QAApplicationAssembly.h"

typedef void (^SuccessBlock)(id successAction);

@protocol QABaseViewControllerProtocol <NSObject>

@property (nonatomic, strong) QADataManager *dataManager;
@property (nonatomic, strong) QARouter *router;
@property (nonatomic, strong) QAApplicationAssembly *assembly;

- (void)showAlertWithError:(QAError *)error
        successActionBlock:(SuccessBlock)successAction;

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
        successActionBlock:(SuccessBlock)successAction;

- (void)showAlertWithTwoButtonTitle:(NSString *)title
                            message:(NSString *)message
                 successActionBlock:(SuccessBlock)successAction;

@end
