//
//  QAAssemblyProtocol.h
//  QuizApp
//
//  Created by Lizunov on 10/18/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class QAMainViewController;
@class QALoginViewController;
@class QACaterogyQuestionsPageViewController;
@class QAHistoryViewController;
@class QAResultViewController;
@class QAChampionshipPageViewController;
@class QADetailedResultViewController;
@class QASingUpViewController;

@protocol QAAssemblyProtocol <NSObject>

- (UIViewController *)initialViewController;
- (QALoginViewController *)assemblyLoginViewControllr;
- (QAMainViewController *)assemblyMainViewController;
- (QASingUpViewController *)assemblySignUpController;
- (QACaterogyQuestionsPageViewController *)assemblyCaterogyQuestionsPageViewController;
- (QADetailedResultViewController *)assemblyDetailedResultViewController;
- (QAHistoryViewController *)assemblyHistoryViewController;
- (QAResultViewController *)assemblyResultViewController;
- (QAChampionshipPageViewController *)assemblyChampionshipViewController;

@end
