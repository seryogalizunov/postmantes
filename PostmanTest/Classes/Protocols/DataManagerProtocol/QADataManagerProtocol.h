//
//  QADataManagerProtocol.h
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAError.h"

@class QAPostRegisterQuery;
@class QAPostLoginQuery;
@class QAPostLogoutQuery;
@class QAGetTokenQuery;
@class QAGetCaterogiesQuery;
@class QAGetCategoryWithQuestionsQuery;
@class QAPostHistoryQuery;
@class QAGetHistoryQuery;
@class QAGetChampionshipQuery;
@class QAServerSideManager;
@class QACoreDataManager;
@class QAUserDefaultsManager;

typedef void (^ServerSideManagerSuccessBlock)(id responseObject);
typedef void (^ServerSideManagerFailureBlock)(QAError *theError);

@protocol QADataManagerProtocol <NSObject>

#pragma mark - 
#pragma mark - Init

- (instancetype)initWithServerSideManager:(QAServerSideManager *)serverSideManager
                          coreDataManager:(QACoreDataManager *)coreDataManager
                       userDefaultManager:(QAUserDefaultsManager *)userDefaultManager;

#pragma mark -
#pragma mark - Post Register

- (void)postRegisterWithQuery:(QAPostRegisterQuery *)query
                 successBlock:(ServerSideManagerSuccessBlock)success
                 failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Post Login

- (void)postLoginWithQuery:(QAPostLoginQuery *)query
              successBlock:(ServerSideManagerSuccessBlock)success
              failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Post Logout

- (void)postLogoutWithQuery:(QAPostLogoutQuery *)query
               successBlock:(ServerSideManagerSuccessBlock)success
               failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get Token

- (void)getTokenWithQuery:(QAGetTokenQuery *)query
             successBlock:(ServerSideManagerSuccessBlock)success
             failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get Catigories

- (void)getCaterogiesWithQuery:(QAGetCaterogiesQuery *)query
                  successBlock:(ServerSideManagerSuccessBlock)success
                  failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get GategoryQuestions

- (void)getCaterogyQuestionsWithQuery:(QAGetCategoryWithQuestionsQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Post History

- (void)postHistoryQuestionsWithQuery:(QAPostHistoryQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get History

- (void)getHistoryQuestionsWithQuery:(QAGetHistoryQuery *)query
                        successBlock:(ServerSideManagerSuccessBlock)success
                        failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get Championship

- (void)getChampionshipQuestionsWithQuery:(QAGetChampionshipQuery *)query
                             successBlock:(ServerSideManagerSuccessBlock)success
                             failureBlock:(ServerSideManagerFailureBlock)failure;

@end
