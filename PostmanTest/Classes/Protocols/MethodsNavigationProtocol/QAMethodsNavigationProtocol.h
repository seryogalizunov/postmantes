//
//  QAMethodsNavigationProtocol.h
//  QuizApp
//
//  Created by Lizunov on 11/3/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIViewController;
@class UINavigationController;

@protocol QAMethodsNavigationProtocol <NSObject>

- (void)performSetNavigationController:(UINavigationController *)navigationController;

- (void)presentViewController:(UIViewController *)viewController;
- (void)pushViewController:(UIViewController *)viewController;
- (void)pushViewControllerWithTransitionFromLeftAnimation:(UIViewController *)viewController;
- (void)popViewController;
- (void)popViewControllerWithTransitionFromRightAnimation;
- (void)popToRootViewController;
- (void)popToRootViewControllerWithTransitionFromRightAnimation;
- (void)dismissViewcontroller:(void (^)(void))block;

@end
