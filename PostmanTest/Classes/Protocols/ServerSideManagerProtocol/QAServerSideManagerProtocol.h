//
//  QAServerSideManagerProtocol.h
//  QuizApp
//
//  Created by Lizunov on 11/1/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QADataManagerProtocol.h"
#import "QAError.h"

@class QANetworkClient;
@class QAPostRegisterQuery;
@class QAPostLoginQuery;
@class QAPostLogoutQuery;
@class QAGetTokenQuery;
@class QAGetCaterogiesQuery;
@class QAGetCategoryWithQuestionsQuery;
@class QAPostHistoryQuery;
@class QAGetHistoryQuery;
@class QAGetChampionshipQuery;

@protocol QAServerSideManagerProtocol <NSObject>

typedef void (^ServerSideManagerSuccessBlock)(id responseObject);
typedef void (^ServerSideManagerFailureBlock)(QAError *theError);

#pragma mark -
#pragma mark - Init NetworkClient

- (instancetype)initWithNetworkClient:(QANetworkClient *)networkClient;

#pragma mark -
#pragma mark - Post Register

- (void)postRegisterWithQuery:(QAPostRegisterQuery *)query
                 successBlock:(ServerSideManagerSuccessBlock)success
                 failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Post Login

- (void)postLoginWithQuery:(QAPostLoginQuery *)query
              successBlock:(ServerSideManagerSuccessBlock)success
              failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Post Logout

- (void)postLogoutWithQuery:(QAPostLogoutQuery *)query
               successBlock:(ServerSideManagerSuccessBlock)success
               failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get Token

- (void)getTokenWithQuery:(QAGetTokenQuery *)query
             successBlock:(ServerSideManagerSuccessBlock)success
             failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get Catigories

- (void)getCaterogiesWithQuery:(QAGetCaterogiesQuery *)query
                  successBlock:(ServerSideManagerSuccessBlock)success
                  failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get GategoryQuestions

- (void)getCaterogyQuestionsWithQuery:(QAGetCategoryWithQuestionsQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Post History

- (void)postHistoryQuestionsWithQuery:(QAPostHistoryQuery *)query
                         successBlock:(ServerSideManagerSuccessBlock)success
                         failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get History

- (void)getHistoryQuestionsWithQuery:(QAGetHistoryQuery *)query
                        successBlock:(ServerSideManagerSuccessBlock)success
                        failureBlock:(ServerSideManagerFailureBlock)failure;

#pragma mark -
#pragma mark - Get Championship

- (void)getChampionshipQuestionsWithQuery:(QAGetChampionshipQuery *)query
                             successBlock:(ServerSideManagerSuccessBlock)success
                             failureBlock:(ServerSideManagerFailureBlock)failure;


@end
