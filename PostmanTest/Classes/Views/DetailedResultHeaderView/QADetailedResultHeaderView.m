//
//  QADetailedResultHeaderView.m
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QADetailedResultHeaderView.h"
#import "QAResultCoreDataModel.h"
#import "QAResult.h"

@implementation QADetailedResultHeaderView

- (void)performLabalWithResult:(QAResult *)result
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([self frame]), 80)];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextColor:[UIColor greenColor]];
    [label setNumberOfLines: 0];
    [label setTextAlignment: NSTextAlignmentCenter];
    [label setBackgroundColor:[UIColor colorWithRed:36.f/255 green:44.f/255 blue:57.f/255 alpha:1.f]];
    
    [label setText:[result question]];
    [self addSubview:label];
}

@end
