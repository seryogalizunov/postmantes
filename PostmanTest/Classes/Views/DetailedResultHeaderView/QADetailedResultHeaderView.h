//
//  QADetailedResultHeaderView.h
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QAResult;

@interface QADetailedResultHeaderView : UIView

- (void)performLabalWithResult:(QAResult *)result;

@end
