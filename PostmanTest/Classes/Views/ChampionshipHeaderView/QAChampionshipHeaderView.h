//
//  QAChampionshipHeaderView.h
//  QuizApp
//
//  Created by Lizunov on 10/25/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QAChampionshipHeaderView : UIView

- (void)performLabalWithText:(NSString *)text;

@end
