//
//  QATestHeaderView.m
//  QuizApp
//
//  Created by Lizunov on 10/25/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QATestHeaderView.h"
#import "QAResultCoreDataModel.h"
#import "QAQuestion.h"

@implementation QATestHeaderView

- (void)performLabalWithQuestion:(QAQuestion *)question
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([self frame]), 80)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    [label setTextColor:[UIColor greenColor]];
    [label setNumberOfLines: 0];
    [label setTextAlignment: NSTextAlignmentCenter];
    [label setBackgroundColor:[UIColor colorWithRed:36.f/255 green:44.f/255 blue:57.f/255 alpha:1.f]];
    
    [label setText:[question question]];
    
    [self addSubview:label];
}

@end
