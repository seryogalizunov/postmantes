//
//  QACategoriesModel.h
//  QuizApp
//
//  Created by Lizunov on 10/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QACategory.h"

@interface QACategories : NSObject

@property (nonatomic, copy, readonly) NSArray<QACategory *> *categories;

- (instancetype)initWithCategoryArray:(NSArray<QACategory *> *)categoryArray;

@end
