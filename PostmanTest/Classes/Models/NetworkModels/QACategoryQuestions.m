//
//  QACategoryQuestions.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QACategoryQuestions.h"

@interface QACategoryQuestions ()

@property (nonatomic, copy, readwrite) NSArray <QAQuestion *> *questions;

@end

@implementation QACategoryQuestions

- (instancetype)initWithQuestionsArray:(NSArray <QAQuestion *> *)questions
{
    self = [super init];
    if (self)
    {
        [self setQuestions:questions];
    }
    return self;
}

@end
