//
//  QAQuestion.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAAnswer.h"

@interface QAQuestion : NSObject

@property (nonatomic, copy, readonly) NSArray <QAAnswer *> *answers;
@property (nonatomic, copy, readonly) NSString *categoryId;
@property (nonatomic, copy, readonly) NSString *objectId;
@property (nonatomic, copy, readonly) NSString *question;
@property (nonatomic, copy, readonly) NSString *rightAnswer;

- (instancetype)initWithAnswersArray:(NSArray<QAAnswer *> *)answersArray
                          categoryId:(NSString *)categoryId
                            objectId:(NSString *)objectId
                            question:(NSString *)question
                         rightAnswer:(NSString *)rightAnswer;
@end
