//
//  QARegister.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QARegister.h"

@interface QARegister ()

@property (nonatomic, copy, readwrite) NSString *email;
@property (nonatomic, copy, readwrite) NSString *login;

@end

@implementation QARegister

- (instancetype)initWithEmaeil:(NSString *)email
                         login:(NSString *)login
{
    self = [super init];
    if (self)
    {
        [self setEmail:email];
        [self setLogin:login];
    }
    return self;
}

@end
