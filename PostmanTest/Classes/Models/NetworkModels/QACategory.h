//
//  QACategory.h
//  QuizApp
//
//  Created by Lizunov on 10/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QACategory : NSObject

@property (nonatomic, copy, readonly) NSString *categoryName;
@property (nonatomic, copy, readonly) NSString *objectId;

- (instancetype)initWithCategoryName:(NSString *)categoryName
                            objectId:(NSString *)objectId;

@end
