//
//  QAResult.h
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QAAnswer.h"

@interface QAResult : NSObject

@property (nonatomic, copy, readonly) NSArray <QAAnswer *> *answers;
@property (nonatomic, copy, readonly) NSString *question;
@property (nonatomic, copy, readonly) NSString *rightAnswer;
@property (nonatomic, copy, readonly) NSString *userAnswer;

- (instancetype)initWithAnswersArray:(NSArray <QAAnswer *> *)answersArray
                            question:(NSString *)quewtion
                         rightAnswer:(NSString *)rightAnswer
                          userAnswer:(NSString *)userAnswer;

@end
