//
//  QACategory.m
//  QuizApp
//
//  Created by Lizunov on 10/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QACategory.h"

@interface QACategory ()

@property (nonatomic, copy, readwrite) NSString *categoryName;
@property (nonatomic, copy, readwrite) NSString *objectId;

@end

@implementation QACategory

- (instancetype)initWithCategoryName:(NSString *)categoryName
                            objectId:(NSString *)objectId
{
    self = [super init];
    if (self)
    {
        [self setCategoryName:categoryName];
        [self setObjectId:objectId];
    }
    return self;
}

@end
