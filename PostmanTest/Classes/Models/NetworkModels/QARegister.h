//
//  QARegister.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QARegister : NSObject

@property (nonatomic, copy, readonly) NSString *email;
@property (nonatomic, copy, readonly) NSString *login;

- (instancetype)initWithEmaeil:(NSString *)email
                         login:(NSString *)login;

@end
