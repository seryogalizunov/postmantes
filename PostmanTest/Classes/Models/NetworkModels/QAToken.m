//
//  QAToken.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAToken.h"

@interface QAToken ()

@property (nonatomic, copy, readwrite) NSString *token;

@end

@implementation QAToken

- (instancetype)initWithToken:(NSString *)token
{
    self = [super init];
    if (self)
    {
        [self setToken:token];
    }
    return self;
}

@end
