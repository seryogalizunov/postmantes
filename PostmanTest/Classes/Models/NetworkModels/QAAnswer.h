//
//  QAAnswer.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface QAAnswer : NSObject

@property (nonatomic, copy, readonly) NSString *answer;

- (instancetype)initWithAnswer:(NSString *)answer;

@end
