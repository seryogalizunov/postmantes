//
//  QACategoryQuestions.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAQuestion.h"

@interface QACategoryQuestions : NSObject

@property (nonatomic, copy, readonly) NSArray <QAQuestion *> *questions;

- (instancetype)initWithQuestionsArray:(NSArray <QAQuestion *> *)questions;

@end
