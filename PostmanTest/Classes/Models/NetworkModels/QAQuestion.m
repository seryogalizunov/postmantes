//
//  QAQuestion.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAQuestion.h"

@interface QAQuestion ()

@property (nonatomic, copy, readwrite) NSArray <QAAnswer *> *answers;
@property (nonatomic, copy, readwrite) NSString *categoryId;
@property (nonatomic, copy, readwrite) NSString *objectId;
@property (nonatomic, copy, readwrite) NSString *question;
@property (nonatomic, copy, readwrite) NSString *rightAnswer;

@end

@implementation QAQuestion

- (instancetype)initWithAnswersArray:(NSArray<QAAnswer *> *)answersArray
                          categoryId:(NSString *)categoryId
                            objectId:(NSString *)objectId
                            question:(NSString *)question
                         rightAnswer:(NSString *)rightAnswer
{
    self = [super init];
    if (self)
    {
        [self setAnswers:answersArray];
        [self setCategoryId:categoryId];
        [self setObjectId:objectId];
        [self setQuestion:question];
        [self setRightAnswer:rightAnswer];
    }
    return self;
}

@end
