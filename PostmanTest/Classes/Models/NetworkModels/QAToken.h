//
//  QAToken.h
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QAToken : NSObject

@property (nonatomic, copy, readonly) NSString *token;

- (instancetype)initWithToken:(NSString *)token;

@end
