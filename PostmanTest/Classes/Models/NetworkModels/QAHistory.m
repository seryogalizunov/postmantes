//
//  QAHistory.m
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAHistory.h"

@interface QAHistory ()

@property (nonatomic, copy, readwrite) NSArray <QAResult *> *results;
@property (nonatomic, copy, readwrite) NSString *categoryName;
@property (nonatomic, strong, readwrite) NSDate *date;

@end

@implementation QAHistory

- (instancetype)initWithResultsModel:(NSArray <QAResult *> *)resultModel
                        categoryName:(NSString *)categoryName
                                date:(NSDate *)date
{
    self = [super init];
    if (self)
    {
        [self setResults:resultModel];
        [self setCategoryName:categoryName];
        [self setDate:date];
    }
    return self;
}

@end
