//
//  QAHistories.m
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAHistories.h"

@interface QAHistories ()

@property (nonatomic, strong, readwrite) NSString *accessToken;
@property (nonatomic, copy, readwrite) NSArray <QAHistory *> *histories;

@end

@implementation QAHistories

- (instancetype)initWithAccessToken:(NSString *)accessToken
                       historyArray:(NSArray<QAHistory *> *)history
{
    self = [super init];
    if (self)
    {
        [self setAccessToken:accessToken];
        [self setHistories:history];
    }
    return self;
}

@end
