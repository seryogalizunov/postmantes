//
//  QAChampionship.m
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAChampionship.h"

@interface QAChampionship ()

@property (nonatomic, copy, readwrite) NSArray <QAQuestion *> *questions;

@end

@implementation QAChampionship

- (instancetype)initWithQuestionsArray:(NSArray<QAQuestion *> *)questions
{
    self = [super init];
    if (self)
    {
        [self setQuestions:questions];
    }
    return self;
}

@end
