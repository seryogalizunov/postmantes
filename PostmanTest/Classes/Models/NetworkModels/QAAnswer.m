//
//  QAAnswer.m
//  QuizApp
//
//  Created by Серега Лизунов on 05.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAAnswer.h"

@interface QAAnswer ()

@property (nonatomic, copy, readwrite) NSString *answer;

@end

@implementation QAAnswer

- (instancetype)initWithAnswer:(NSString *)answer
{
    self = [super init];
    if (self)
    {
        [self setAnswer:answer];
    }
    return self;
}

@end
