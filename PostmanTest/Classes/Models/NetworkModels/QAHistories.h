//
//  QAHistories.h
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAHistory.h"

@interface QAHistories : NSObject

@property (nonatomic, strong, readonly) NSString *accessToken;
@property (nonatomic, copy, readonly) NSArray <QAHistory *> *histories;

- (instancetype)initWithAccessToken:(NSString *)accessToken historyArray:(NSArray <QAHistory *> *)history;

@end
