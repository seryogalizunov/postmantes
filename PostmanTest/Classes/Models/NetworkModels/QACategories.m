//
//  QACategoriesModel.m
//  QuizApp
//
//  Created by Lizunov on 10/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QACategories.h"

@interface QACategories ()

@property (nonatomic, copy, readwrite) NSArray<QACategory *> *categories;

@end

@implementation QACategories

- (instancetype)initWithCategoryArray:(NSArray<QACategory *> *)categoryArray
{
    self = [super init];
    if (self)
    {
        [self setCategories:categoryArray];
    }
    return self;
}

@end
