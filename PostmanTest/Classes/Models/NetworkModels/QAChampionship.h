//
//  QAChampionship.h
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAQuestion.h"

@interface QAChampionship : NSObject

@property (nonatomic, copy, readonly) NSArray <QAQuestion *> *questions;

- (instancetype)initWithQuestionsArray:(NSArray <QAQuestion *> *)questions;

@end
