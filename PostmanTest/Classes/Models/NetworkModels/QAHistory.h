//
//  QAHistory.h
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QAResult.h"

@interface QAHistory : NSObject

@property (nonatomic, copy, readonly) NSArray <QAResult *> *results;
@property (nonatomic, copy, readonly) NSString *categoryName;
@property (nonatomic, strong, readonly) NSDate *date;

- (instancetype)initWithResultsModel:(NSArray <QAResult *> *)resultModel
                        categoryName:(NSString *)categoryName
                                date:(NSDate *)date;
@end
