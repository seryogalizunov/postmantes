//
//  QAResult.m
//  QuizApp
//
//  Created by Серега Лизунов on 06.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAResult.h"

@interface QAResult ()

@property (nonatomic, copy, readwrite) NSArray <QAAnswer *> *answers;
@property (nonatomic, copy, readwrite) NSString *question;
@property (nonatomic, copy, readwrite) NSString *rightAnswer;
@property (nonatomic, copy, readwrite) NSString *userAnswer;

@end

@implementation QAResult

- (instancetype)initWithAnswersArray:(NSArray<QAAnswer *> *)answersArray
                            question:(NSString *)quewtion
                         rightAnswer:(NSString *)rightAnswer
                          userAnswer:(NSString *)userAnswer
{
    self = [super init];
    if (self)
    {
        [self setAnswers:answersArray];
        [self setQuestion:quewtion];
        [self setRightAnswer:rightAnswer];
        [self setUserAnswer:userAnswer];
    }
    return self;
}

@end
