//
//  QAHistoryCoreDataModel+CoreDataProperties.m
//  
//
//  Created by Lizunov on 11/1/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QAHistoryCoreDataModel+CoreDataProperties.h"

@implementation QAHistoryCoreDataModel (CoreDataProperties)

@dynamic categoryName;
@dynamic date;
@dynamic histories;
@dynamic results;

@end
