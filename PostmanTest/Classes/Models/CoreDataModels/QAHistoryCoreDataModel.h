//
//  QAHistoryCoreDataModel.h
//  
//
//  Created by Lizunov on 11/1/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QAHistoriesCoreDataModel, QAResultCoreDataModel;

NS_ASSUME_NONNULL_BEGIN

@interface QAHistoryCoreDataModel : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "QAHistoryCoreDataModel+CoreDataProperties.h"
