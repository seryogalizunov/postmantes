//
//  QAResultCoreDataModel+CoreDataProperties.m
//  
//
//  Created by Lizunov on 11/1/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QAResultCoreDataModel+CoreDataProperties.h"

@implementation QAResultCoreDataModel (CoreDataProperties)

@dynamic question;
@dynamic rightAnswer;
@dynamic userAnswer;
@dynamic answers;
@dynamic history;

@end
