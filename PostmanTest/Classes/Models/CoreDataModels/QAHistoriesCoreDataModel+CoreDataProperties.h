//
//  QAHistoriesCoreDataModel+CoreDataProperties.h
//  
//
//  Created by Lizunov on 11/1/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QAHistoriesCoreDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QAHistoriesCoreDataModel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *accessToken;
@property (nullable, nonatomic, retain) NSSet<QAHistoryCoreDataModel *> *histories;

@end

@interface QAHistoriesCoreDataModel (CoreDataGeneratedAccessors)

- (void)addHistoriesObject:(QAHistoryCoreDataModel *)value;
- (void)removeHistoriesObject:(QAHistoryCoreDataModel *)value;
- (void)addHistories:(NSSet<QAHistoryCoreDataModel *> *)values;
- (void)removeHistories:(NSSet<QAHistoryCoreDataModel *> *)values;

@end

NS_ASSUME_NONNULL_END
