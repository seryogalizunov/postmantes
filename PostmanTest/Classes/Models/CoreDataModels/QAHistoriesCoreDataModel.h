//
//  QAHistoriesCoreDataModel.h
//  
//
//  Created by Lizunov on 11/1/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QAHistoryCoreDataModel;

NS_ASSUME_NONNULL_BEGIN

@interface QAHistoriesCoreDataModel : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "QAHistoriesCoreDataModel+CoreDataProperties.h"
