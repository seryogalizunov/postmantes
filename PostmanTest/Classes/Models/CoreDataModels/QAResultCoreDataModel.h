//
//  QAResultCoreDataModel.h
//  
//
//  Created by Lizunov on 11/1/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QAHistoryCoreDataModel;

NS_ASSUME_NONNULL_BEGIN

@interface QAResultCoreDataModel : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "QAResultCoreDataModel+CoreDataProperties.h"
