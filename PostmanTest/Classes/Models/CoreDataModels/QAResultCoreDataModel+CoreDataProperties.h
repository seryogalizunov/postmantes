//
//  QAResultCoreDataModel+CoreDataProperties.h
//  
//
//  Created by Lizunov on 11/1/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QAResultCoreDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QAResultCoreDataModel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *question;
@property (nullable, nonatomic, retain) NSString *rightAnswer;
@property (nullable, nonatomic, retain) NSString *userAnswer;
@property (nullable, nonatomic, retain) NSData *answers;
@property (nullable, nonatomic, retain) QAHistoryCoreDataModel *history;

@end

NS_ASSUME_NONNULL_END
