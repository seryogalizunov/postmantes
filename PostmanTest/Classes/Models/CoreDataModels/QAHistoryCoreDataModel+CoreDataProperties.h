//
//  QAHistoryCoreDataModel+CoreDataProperties.h
//  
//
//  Created by Lizunov on 11/1/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QAHistoryCoreDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QAHistoryCoreDataModel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *categoryName;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) QAHistoriesCoreDataModel *histories;
@property (nullable, nonatomic, retain) NSSet<QAResultCoreDataModel *> *results;

@end

@interface QAHistoryCoreDataModel (CoreDataGeneratedAccessors)

- (void)addResultsObject:(QAResultCoreDataModel *)value;
- (void)removeResultsObject:(QAResultCoreDataModel *)value;
- (void)addResults:(NSSet<QAResultCoreDataModel *> *)values;
- (void)removeResults:(NSSet<QAResultCoreDataModel *> *)values;

@end

NS_ASSUME_NONNULL_END
