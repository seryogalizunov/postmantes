//
//  QABaseVIewController.m
//  QuizApp
//
//  Created by Lizunov on 10/21/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseViewController.h"

@implementation QABaseViewController

@synthesize dataManager;
@synthesize router;
@synthesize assembly;

#pragma mark -
#pragma mark - ViewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
}

#pragma mark -
#pragma mark - Show Alert With Error

- (void)showAlertWithError:(QAError *)error
        successActionBlock:(SuccessBlock)successAction
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                                             message:[[error userInfo] objectForKey:@"error"]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:successAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - 
#pragma mark - Show Alert With Title, Message and SuccessAction 

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
        successActionBlock:(SuccessBlock)successAction
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:successAction];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlertWithTwoButtonTitle:(NSString *)title
                            message:(NSString *)message
                 successActionBlock:(SuccessBlock)successAction
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:NULL];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Stop"
                                                       style:UIAlertActionStyleDefault
                                                     handler:successAction];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
