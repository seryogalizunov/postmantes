//
//  QABaseVIewController.h
//  QuizApp
//
//  Created by Lizunov on 10/21/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseViewControllerProtocol.h"

@interface QABaseViewController : UIViewController <QABaseViewControllerProtocol>

@end
