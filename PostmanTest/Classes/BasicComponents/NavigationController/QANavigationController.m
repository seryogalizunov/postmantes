//
//  QANavigationController.m
//  QuizApp
//
//  Created by Lizunov on 9/21/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QANavigationController.h"

@implementation QANavigationController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationItem] setHidesBackButton:YES animated:NO];
    [[self view] setBackgroundColor:[UIColor colorWithRed:36.f/255 green:44.f/255 blue:57.f/255 alpha:1.f]];
    
    [self performNavigationBarTitleColor];
}

- (void)performNavigationBarTitleColor
{
    [[self navigationBar] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[self navigationBar] setTranslucent:NO];
    [[self navigationBar] setBarTintColor:[UIColor colorWithRed:36.f/255 green:44.f/255 blue:57.f/255 alpha:1.f]];
}

@end
