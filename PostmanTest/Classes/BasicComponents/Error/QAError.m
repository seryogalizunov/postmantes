//
//  QAError.m
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAError.h"
#import "AFNetworking.h"

@implementation QAError

#pragma mark -
#pragma mark - Parsing NSError

- (QAError *)parsingJSONError:(NSError *)error
{
    if ([[error localizedDescription] isEqualToString:@"Could not connect to the server."])
    {
        QAError *resultError = [[QAError alloc] initWithDomain:@"" code:[error code] userInfo:@{@"error" : [error localizedDescription]}];
        return resultError;
    }
    else
    {
        QAError *resultError = [[QAError alloc] initWithDomain:@"" code:[error code] userInfo:@{@"error" : [self resulStringWithError:error]}];
        return resultError;
    }
}

#pragma mark -
#pragma mark - Result string with error

- (NSString *)resulStringWithError:(NSError *)error
{
    NSDictionary<NSString *, id> *responseError = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:0 error:nil];
    
    if ([responseError objectForKey:@"error:"])
    {
        responseError = responseError[@"error:"];
    }
    else if ([responseError objectForKey:@"error"])
    {
        responseError = responseError[@"error"];
    }
    return [NSString stringWithFormat:@"%@", responseError];
}

@end
