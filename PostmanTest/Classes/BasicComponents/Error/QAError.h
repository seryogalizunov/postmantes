//
//  QAError.h
//  QuizApp
//
//  Created by Lizunov on 9/30/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAConectionCommandProtocol.h"

@interface QAError : NSError 

- (QAError *)parsingJSONError:(NSError *)error;

@end
