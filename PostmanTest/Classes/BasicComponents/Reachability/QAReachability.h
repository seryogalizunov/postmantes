//
//  QAReachability.h
//  QuizApp
//
//  Created by Lizunov on 10/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QAReachability : NSObject

+ (BOOL)hasConnectivity;
+ (BOOL)isNetworkAvailable;

@end
