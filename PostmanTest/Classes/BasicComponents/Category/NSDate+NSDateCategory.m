//
//  NSDate+NSDateCategory.m
//  QuizApp
//
//  Created by Lizunov on 11/1/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "NSDate+NSDateCategory.h"

@implementation NSDate (NSDateCategory)

#pragma mark -
#pragma mark - Convert NSTimeInterval to NSDate

- (NSDate *)convertTimeIntervalToDate:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    return date;
}

- (NSString *)converToString
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:self
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    return dateString;
}

#pragma mark -
#pragma mark - Convert NSDate to NSTimeInterval

- (NSTimeInterval)converToTimeInterval {
    return [self timeIntervalSince1970];
}

#pragma mark -
#pragma mark - Return StringWithTimeInterval

- (NSString *)returnStringWithTimeInterval:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

@end
