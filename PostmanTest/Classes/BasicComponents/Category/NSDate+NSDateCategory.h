//
//  NSDate+NSDateCategory.h
//  QuizApp
//
//  Created by Lizunov on 11/1/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (NSDateCategory)

- (NSString *)converToString;
- (NSTimeInterval)converToTimeInterval;
- (NSDate *)convertTimeIntervalToDate:(NSTimeInterval)timeInterval;
- (NSString *)returnStringWithTimeInterval:(NSTimeInterval)timeInterval;

@end
