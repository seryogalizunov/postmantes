//
//  QALoginVIewController.m
//  QuizApp
//
//  Created by Lizunov on 9/16/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QALoginViewController.h"
#import "QARequestAction.h"
#import "QAPostRegisterQuery.h"
#import "QAPostLoginQuery.h"
#import "QARegister.h"
#import "QAToken.h"
#import "QARouter.h"
#import "QADataManager.h"
#import "QAGetCaterogiesQuery.h"
#import "QAGetHistoryQuery.h"

@interface QALoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation QALoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[self navigationController] navigationBar] setHidden:YES];
}

#pragma mark -
#pragma makr - Action

- (IBAction)signUp:(UIButton *)sender
{
    QAPostRegisterQuery *postRegister = [[QAPostRegisterQuery alloc] init];
    [postRegister setEmail:[[self emailTextField] text]];
    [postRegister setPassword:[[self passwordTextField] text]];
    
    [[self dataManager] postRegisterWithQuery:postRegister
                                 successBlock:^(id responseObject)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        [[weakSelf router] showSignUpController];
                                    }
                                 failureBlock:^(QAError *theError)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        [weakSelf showAlertWithError:theError
                                                  successActionBlock:NULL];
                                    }];
}

- (IBAction)signIn:(UIButton *)sender
{
    QAPostLoginQuery *postLogin = [[QAPostLoginQuery alloc] init];
    [postLogin setEmail:[[self emailTextField] text]];
    [postLogin setPassword:[[self passwordTextField] text]];
    
    [[self dataManager] postLoginWithQuery:postLogin
                              successBlock:^(id responseObject)
                                {
                                    __weak typeof(self) weakSelf = self;
                                    [[weakSelf router] popViewControllerWithTransitionFromRightAnimation];
                                }
                              failureBlock:^(QAError *theError)
                                {
                                    __weak typeof(self) weakSelf = self;
                                    [weakSelf showAlertWithError:theError
                                              successActionBlock:NULL];
                                }];
}

#pragma mark -
#pragma mark - Hide Keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)viewTapped:(UITapGestureRecognizer *)sender
{
    [[self emailTextField] resignFirstResponder];
    [[self passwordTextField] resignFirstResponder];
}

@end
