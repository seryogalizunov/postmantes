//
//  QADetailedResultViewController.m
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QADetailedResultViewController.h"
#import "QAResultCoreDataModel.h"
#import "QAResult.h"
#import "QADetailedResultViewControllerDataSource.h"
#import "QADetailedResultViewTableViewDelegate.h"


@interface QADetailedResultViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) QADetailedResultViewControllerDataSource *dataSource;
@property (nonatomic, strong) QADetailedResultViewTableViewDelegate *delegate;

@end

@implementation QADetailedResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationItem] setHidesBackButton:NO animated:NO];
    [self performTableView];
}

#pragma mark -
#pragma mark - performTableView

- (void)performTableView
{
    [self setDataSource:[[QADetailedResultViewControllerDataSource alloc] initWithResult:[self result]]];
    [self setDelegate:[[QADetailedResultViewTableViewDelegate alloc] initWithResult:[self result]]];
    
    [[self tableView] setDataSource:[self dataSource]];
    [[self tableView] setDelegate:[self delegate]];
    [[self tableView] registerNib:[UINib nibWithNibName:@"QADetailedResultViewCell" bundle:nil] forCellReuseIdentifier:@"QADetailedResultViewCell"];
    [[self tableView] reloadData];
} 

@end
