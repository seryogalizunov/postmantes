//
//  QADetailedResultViewController.h
//  QuizApp
//
//  Created by Серега Лизунов on 30.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseViewController.h"

@class QAResult;

@interface QADetailedResultViewController : QABaseViewController

@property (nonatomic, strong) QAResult *result;

@end
