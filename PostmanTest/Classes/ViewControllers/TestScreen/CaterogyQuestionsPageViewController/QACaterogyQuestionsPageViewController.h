//
//  QATestPageViewController.h
//  QuizApp
//
//  Created by Lizunov on 9/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseViewController.h"

@class QACategory;

@interface QACaterogyQuestionsPageViewController : QABaseViewController

@property (nonatomic, strong) QACategory *category;

@end
