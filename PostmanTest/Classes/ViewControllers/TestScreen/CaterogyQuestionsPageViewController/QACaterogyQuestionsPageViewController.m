//
//  QATestPageViewController.m
//  QuizApp
//
//  Created by Lizunov on 9/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QACaterogyQuestionsPageViewController.h"
#import "QATestViewController.h"
#import "QACategory.h"
#import "QAGetCategoryWithQuestionsQuery.h"
#import "QACategoryQuestions.h"
#import "QAPostHistoryQuery.h"
#import "QARouter.h"
#import "QADataManager.h"
#import "QAGetHistoryQuery.h"
#import "QAHistories.h"
#import "QAUserDefaultsManager.h"

@interface QACaterogyQuestionsPageViewController () <QATestViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) UIButton * timerButton;

@property (nonatomic, strong) NSMutableArray *requestsArray;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger seconds;

@property (nonatomic, strong) QACategoryQuestions *categoriesQuestions;
@property (nonatomic, strong) QAGetHistoryQuery *historyQuery;

@end

@implementation QACaterogyQuestionsPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setRequestsArray:[[NSMutableArray alloc] init]];
    [[self navigationItem] setTitle:[[self category] categoryName]];
    
    [self setPageControl];
    [self setPageViewController:[[self storyboard] instantiateViewControllerWithIdentifier:@"QATestPageController"]];
    
    for (UIGestureRecognizer *recognizer in [[self pageViewController] gestureRecognizers])
    {
        [recognizer setEnabled:NO];
    }
    
    [self getCaterogyQuestions];
    [self createTimerButton];
    [self setTimer];
}

- (void)setPageControl
{
    UIPageControl *pageControl = [UIPageControl appearance];
    [pageControl setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [pageControl setCurrentPageIndicatorTintColor:[UIColor blackColor]];
    [pageControl setBackgroundColor:[UIColor whiteColor]];
}

- (void)getCaterogyQuestions
{
    QAGetCategoryWithQuestionsQuery *getCategoryQuestions = [[QAGetCategoryWithQuestionsQuery alloc] init];
    [getCategoryQuestions setWhere:[[self category] objectId]];
    [getCategoryQuestions setOffset:@"0"];
    [getCategoryQuestions setPageSize:@"9"];
    
    [[self dataManager] getCaterogyQuestionsWithQuery:getCategoryQuestions
                                         successBlock:^(id responseObject)
                                            {
                                                __weak typeof(self) weakSelf = self;
                                                [weakSelf setCategoriesQuestions:responseObject[NSStringFromClass([QACategoryQuestions class])]];
                                                [weakSelf createPageViewControllerWithIndexPage:0];
                                            }
                                         failureBlock:^(QAError *theError)
                                            {
                                                __weak typeof(self) weakSelf = self;
                                                [weakSelf showAlertWithError:theError
                                                          successActionBlock:^(id successAction)
                                                            {
                                                                [weakSelf successAction];
                                                            }];
                                            }];
}

- (void)setTimer
{
    [self setSeconds:600];
    [self setTimer:[NSTimer scheduledTimerWithTimeInterval:1.0f
                                                    target:self
                                                  selector:@selector(updateTimerAction:)
                                                  userInfo:nil
                                                   repeats:YES]];
}

- (void)createTimerButton
{
    [self setTimerButton:[UIButton buttonWithType:UIButtonTypeCustom]];
    [[self timerButton] setFrame:CGRectMake(0, 0, 50, CGRectGetHeight([[self view] bounds]))];
    [[self timerButton] setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [[self timerButton] setExclusiveTouch:YES];
    [[self timerButton] setTitle:@"10:00" forState:UIControlStateNormal];
    [[self timerButton] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIBarButtonItem *backMenuBarButton = [[UIBarButtonItem alloc] initWithCustomView:[self timerButton]];
    [[self navigationItem] setLeftBarButtonItem:backMenuBarButton];
}

#pragma mark
#pragma mark - Create PageViewController

- (void)createPageViewControllerWithIndexPage:(NSInteger)indexPage
{
    QATestViewController *startingViewController = [self viewControllerAtIndex:indexPage];
    [startingViewController setDelegate:self];
    NSArray *viewControllers = @[startingViewController];
    [[self pageViewController] setViewControllers:viewControllers
                                        direction:UIPageViewControllerNavigationDirectionForward
                                         animated:YES
                                       completion:nil];
        
    [[[self pageViewController] view] setFrame:CGRectMake(0, 0, CGRectGetWidth([[self view] frame]), CGRectGetHeight([[self view] frame]))];
    
    [self addChildViewController:[self pageViewController]];
    [[self view] addSubview:[[self pageViewController] view]];
    [[self pageViewController] didMoveToParentViewController:self];
}

- (QATestViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([[[self categoriesQuestions] questions] count] == 0))
    {
        return nil;
    }
    
    QATestViewController *pageContentViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"QATestViewController"];
    [pageContentViewController setQuestion:[[[self categoriesQuestions] questions] objectAtIndex:index]];
    [pageContentViewController setPageIndex:index];
    
    return pageContentViewController;
}

#pragma mark
#pragma mark - QATestViewControllerDelegate

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary
{
    [[self requestsArray] addObject:dictionary];
        
    if ([[[self categoriesQuestions] questions] count] > indexPage)
    {
        [self createPageViewControllerWithIndexPage:indexPage];
    }
    else if ([[[self categoriesQuestions] questions] count] <= indexPage)
    {
        [self performPostHistoryRequest];
    }
}

#pragma mark -
#pragma mark - performPostHistoryRequest

- (void)performPostHistoryRequest
{
    if ([[self requestsArray] count] > 0)
    {
        QAPostHistoryQuery *postHistory = [[QAPostHistoryQuery alloc] init];
        [postHistory setQuestionsArray:[self requestsArray]];
        [postHistory setCategoryName:[[self category] categoryName]];
        [postHistory setDateInTimeInterval:[[NSDate date] timeIntervalSince1970]];
        
        [[self dataManager] postHistoryQuestionsWithQuery:postHistory
                                             successBlock:^(id responseObject)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf setHistoryQuery:[[QAGetHistoryQuery alloc] init]];
                                                    [[weakSelf dataManager] getHistoryQuestionsWithQuery:[weakSelf historyQuery]
                                                                                            successBlock:^(id responseObject)
                                                                                            {
                                                                                                [weakSelf showAlertWithTitle:@"Test is finished"
                                                                                                                     message:@"View You Result in History Screen"
                                                                                                          successActionBlock:^(id successAction)
                                                                                                            {
                                                                                                                [weakSelf successAction];
                                                                                                            }];
                                                                                            }
                                                                                            failureBlock:^(QAError *theError)
                                                                                            {
                                                                                                [weakSelf showAlertWithError:theError
                                                                                                          successActionBlock:^(id successAction)
                                                                                                            {
                                                                                                                [weakSelf successAction];
                                                                                                            }];
                                                                                            }];
                                                }
                                             failureBlock:^(QAError *theError)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf showAlertWithTitle:@"Error"
                                                                         message:[[theError userInfo] objectForKey:@"error"]
                                                              successActionBlock:^(id successAction)
                                                                {
                                                                    [weakSelf successAction];
                                                                }];
                                                }];
    }
}

#pragma mark
#pragma mark - Action

- (void)updateTimerAction:(NSTimer *)theTimer
{
    if ([self seconds] > 0)
    {
        self.seconds = self.seconds - 1;
        NSInteger minutes = (NSInteger)[self seconds] / 60;
        NSInteger seconds = [self seconds] % 60;
        [[self timerButton] setTitle:[NSString stringWithFormat:@"%ld:%02ld", (long)minutes, (long)seconds]
                            forState:UIControlStateNormal];
    }
    else
    {
        [self performPostHistoryRequest];
        [[self timer] invalidate];
        
        [self showAlertWithTitle:@"Finish"
                         message:@"Time is over"
              successActionBlock:^(id successAction)
                {
                    __weak typeof(self) weakSelf = self;
                    [weakSelf successAction];
                }];
    }
}

- (IBAction)stopAction:(UIBarButtonItem *)sender
{
    [self showAlertWithTwoButtonTitle:@"Stop test"
                              message:@"Stop this test?"
                   successActionBlock:^(id successAction)
                    {
                        __weak typeof(self) weakSelf = self;
                        [weakSelf successAction];
                    }];
}

#pragma mark - 
#pragma mark - Success Action

- (void)successAction
{
    [[self timer] invalidate];
    [self setTimer:nil];
    [[self router] popViewController];
}

@end
