//
//  QATestViewController.m
//  QuizApp
//
//  Created by Lizunov on 9/22/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QATestViewController.h"
#import "QATestCell.h"
#import "QAAnswer.h"
#import "QAQuestion.h"
#import "QAResult.h"
#import "QATestHeaderView.h"
#import "QATestTableViewDelegate.h"
#import "QATestViewControllerDataSource.h"

@interface QATestViewController () <QATestDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) QATestViewControllerDataSource *dataSource;
@property (nonatomic, strong) QATestTableViewDelegate *testDelegate;

@end

@implementation QATestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationItem] setHidesBackButton:NO animated:NO];
    [self performTableView];
}

#pragma mark -
#pragma mark - performTableView

- (void)performTableView
{
    [self setDataSource:[[QATestViewControllerDataSource alloc] initWithQuestion:[self question]]];
    [self setTestDelegate:[[QATestTableViewDelegate alloc] initWithQuestion:[self question]]];
    
    [[self tableView] setDataSource:[self dataSource]];
    [[self tableView] setDelegate:[self testDelegate]];
    [[self testDelegate] setDelegate:self];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"QATestCell" bundle:nil] forCellReuseIdentifier:@"QATestCell"];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark - QATestDelegate

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary
{
    if ([[self delegate] respondsToSelector:@selector(nextPage:requestsDictionary:)])
    {
        [[self delegate] nextPage:([self pageIndex] + 1)
               requestsDictionary:dictionary];
    }
}

@end
