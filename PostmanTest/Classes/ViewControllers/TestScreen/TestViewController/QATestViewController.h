//
//  QATestViewController.h
//  QuizApp
//
//  Created by Lizunov on 9/22/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAResultCoreDataModel.h"
#import "QABaseViewController.h"

@class QAQuestion;
@class QAResultNetworkModel;

@protocol QATestViewControllerDelegate <NSObject>

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary;

@end

@interface QATestViewController : QABaseViewController

@property (nonatomic, strong) QAQuestion *question;
@property (nonatomic, assign) NSUInteger pageIndex;

@property (nonatomic, weak) id<QATestViewControllerDelegate> delegate;

@end
