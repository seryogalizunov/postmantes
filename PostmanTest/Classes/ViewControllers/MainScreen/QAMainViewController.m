//
//  QAMainViewController.m
//  QuizApp
//
//  Created by Lizunov on 9/16/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "QAMainViewController.h"
#import "QALoginVIewController.h"
#import "QACaterogyQuestionsPageViewController.h"
#import "QAHistoryViewController.h"
#import "QAChampionshipPageViewController.h"
#import "QARouter.h"

#import "QARequestAction.h"
#import "QAGetTokenQuery.h"
#import "QAGetCaterogiesQuery.h"
#import "QACategory.h"
#import "QACategories.h"
#import "QAPostLogoutQuery.h"
#import "QAGetHistoryQuery.h"

#import "QACoreDataManager.h"
#import "QADataManager.h"

#import "QAMainViewControllerDataSource.h"
#import "QALoginVIewController.h"
#import "QAMainTableViewDelegate.h"
#import "QAUserDefaultsManager.h"

@interface QAMainViewController () <QAMainDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *alertView;

@property (nonatomic, strong) QACategories *categories;
@property (nonatomic, strong) QACategory *category;
@property (nonatomic, strong) QAGetHistoryQuery *historyQuery;

@property (nonatomic, strong) QAMainViewControllerDataSource *dataSource;
@property (nonatomic, strong) QAMainTableViewDelegate *delegate;

@end

@implementation QAMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self alertView] setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[[self navigationController] navigationBar] setHidden:NO];
    [[self navigationItem] setHidesBackButton:YES animated:NO];
    
    [self performGetTestCategory];
}

#pragma mark -
#pragma mark - perform GetHistory

- (void)performGetTestCategory
{
    QAGetCaterogiesQuery *getCategory = [[QAGetCaterogiesQuery alloc] init];
    [[self dataManager] getCaterogiesWithQuery:getCategory
                                  successBlock:^(id responseObject)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        [weakSelf performTableViewWithCateroies:responseObject];
                                    }
                                  failureBlock:^(QAError *theError)
                                    {
                                        __weak typeof(self) weakSelf = self;
                                        [weakSelf showInternetErrorOrServerError:theError];
                                    }];
}

#pragma mark -
#pragma mark - performTableViewWithCateroies:

- (void)performTableViewWithCateroies:(id)categories
{
    [self setCategories:categories[NSStringFromClass([QACategories class])]];
    [[self tableView] setHidden:NO];
    [[self alertView] setHidden:YES];
    
    [self setDataSource:[[QAMainViewControllerDataSource alloc] initWithCategory:[self categories]]];
    [self setDelegate:[[QAMainTableViewDelegate alloc] initWithCategory:[self categories]]];
    [[self delegate] setDelegete:self];
    [[self tableView] setDataSource:[self dataSource]];
    [[self tableView] setDelegate:[self delegate]];
    [[self tableView] registerNib:[UINib nibWithNibName:@"QACategoriesTestCell" bundle:nil] forCellReuseIdentifier:@"QACategoriesTestCell"];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark - ShowAlertViewOrAler

- (void)showInternetErrorOrServerError:(QAError *)theError
{
    if ([[[theError userInfo] objectForKey:@"error"] isEqualToString:@"Could not connect to the server."])
    {
        [[self tableView] setHidden:YES];
        [[self alertView] setHidden:NO];
    }
    else
    {
        [self showAlertWithError:theError
              successActionBlock:^(id successAction)
                {
                    __weak typeof(self) weakSelf = self;
                    [[weakSelf router] showLoginViewController];
                }];
    }
}

#pragma mark
#pragma mark - UIButton Action

- (IBAction)historyButtonAction:(UIButton *)sender
{
    [[self router] showHistoryViewController];
}

- (IBAction)championshipButtonAction:(UIButton *)sender
{
    [[self router] showChampionshipViewController];
}

- (IBAction)logoutButtonAction:(UIButton *)sender
{
    QAPostLogoutQuery *logout = [[QAPostLogoutQuery alloc] init];
    [[self dataManager] postLogoutWithQuery:logout
                               successBlock:^(id responseObject)
                                {
                                    __weak typeof(self) weakSelf = self;
                                    [[weakSelf router] showLoginViewController];
                                }
                               failureBlock:^(QAError *theError)
                                {
                                   __weak typeof(self) weakSelf = self;
                                    [weakSelf showAlertWithError:theError
                                              successActionBlock:NULL];
                               }];
}

#pragma mark -
#pragma mark - Show Alert

- (void)showAlertWithCategory:(QACategory *)category
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString  stringWithFormat:@"Start %@", [category categoryName]]
                                                                             message:@"Start this test?"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:NULL];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Start"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                                                        {
                                                            __weak typeof(self) weakSelf = self;
                                                            [weakSelf setCategory:category];
                                                            [[weakSelf router] showCaterogyQuestionsPageViewControllerWithCategory:category];
                                                        }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
