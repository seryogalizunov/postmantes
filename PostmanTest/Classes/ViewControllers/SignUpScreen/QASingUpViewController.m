//
//  QASingUpViewCintroller.m
//  QuizApp
//
//  Created by Lizunov on 11/4/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QASingUpViewController.h"
#import "QAPostLoginQuery.h"

@interface QASingUpViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation QASingUpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[self navigationController] navigationBar] setHidden:YES];
}

#pragma mark -
#pragma makr - Action

- (IBAction)signIn:(UIButton *)sender
{
    QAPostLoginQuery *postLogin = [[QAPostLoginQuery alloc] init];
    [postLogin setEmail:[[self emailTextField] text]];
    [postLogin setPassword:[[self passwordTextField] text]];
    
    [[self dataManager] postLoginWithQuery:postLogin
                              successBlock:^(id responseObject)
                                {
                                    __weak typeof(self) weakSelf = self;
                                    [[weakSelf router] popToRootViewControllerWithTransitionFromRightAnimation];
                                }
                              failureBlock:^(QAError *theError)
                                {
                                    __weak typeof(self) weakSelf = self;
                                    [weakSelf showAlertWithError:theError
                                              successActionBlock:NULL];
                                }];
}

#pragma mark -
#pragma mark - Hide Keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)viewTapped:(UITapGestureRecognizer *)sender
{
    [[self emailTextField] resignFirstResponder];
    [[self passwordTextField] resignFirstResponder];
}

@end
