//
//  QAResultViewController.m
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAResultViewController.h"
#import "QAHistory.h"
#import "QAResultCell.h"
#import "QATestViewController.h"
#import "QARouter.h"
#import "QADataManager.h"
#import "QAResultViewControllerDataSource.h"
#import "QAResultCollectionViewDelegate.h"
#import "QADetailedResultViewController.h"

@interface QAResultViewController () <QAResultDelegete>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *totalPercentLable;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleLabel;

@property (nonatomic, strong) QAResult *result;

@property (nonatomic, strong) QAResultViewControllerDataSource *dataSource;
@property (nonatomic, strong) QAResultCollectionViewDelegate *delegate;

@end

@implementation QAResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[self navigationController] navigationBar] setHidden:NO];
    [[self navigationItem] setHidesBackButton:NO animated:YES];
    
    [self setDataSource:[[QAResultViewControllerDataSource alloc] initWithHistoryNetworkModel:[self history]]];
    [self setDelegate:[[QAResultCollectionViewDelegate alloc] initWithHistory:[self history]]];
    
    [[self collectionView] setDelegate:[self delegate]];
    [[self collectionView] setDataSource:[self dataSource]];
    [[self delegate] setDelegate:self];
    [[self collectionView] registerNib:[UINib nibWithNibName:@"QAResultCell" bundle:nil] forCellWithReuseIdentifier:@"ResultCell"];
    
    [[self resultTitleLabel] setText:[self resultTitleStringWithHistory:[self history]]];
    [[self totalPercentLable] setText:[self totalResulrInPercentWithHistory:[self history]]];
    [[self collectionView] reloadData];
}

#pragma mark -
#pragma mark - QAResultDelegete

- (void)performResult:(QAResult *)result
{
    [self setResult:result];
    [[self router] showDetailedResultViewControllerWithResult:result];
}

#pragma mark -
#pragma mark - Private Methods

- (NSString *)resultTitleStringWithHistory:(QAHistory *)history
{
    return [NSString stringWithFormat:@"Your %@ Result", [history categoryName]];
}

- (NSString *)totalResulrInPercentWithHistory:(QAHistory *)history

{
    NSInteger result = [self countRightAnswers:history];
    return [NSString stringWithFormat:@"Your Total %ld%%", (long)result];
}

- (NSInteger)countRightAnswers:(QAHistory *)history
{
    NSInteger result = 0;
    
    for (NSInteger index = 0; index < [[history results] count]; index++)
    {
        QAResult *resultModel = [[history results] objectAtIndex:index];
        NSInteger userAnswer =[[resultModel userAnswer] integerValue];
        NSInteger rightAnswer = [[resultModel rightAnswer] integerValue];
        NSInteger countAnswers = [[resultModel answers] count];
        
        if (userAnswer == rightAnswer)
        {
            result++;
        }
        else if (((userAnswer + 1) == rightAnswer) && (rightAnswer == countAnswers))
        {
            result++;
        }
    }
    result = result * 100 / [[history results] count];
    return result;
}

@end
