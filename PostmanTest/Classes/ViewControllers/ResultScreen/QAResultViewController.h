//
//  QAResultViewController.h
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseViewController.h"

@class QAHistory;

@interface QAResultViewController : QABaseViewController

@property (nonatomic, strong) QAHistory *history;

@end
