//
//  QAChampionshipPageViewController.m
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAChampionshipPageViewController.h"
#import "QAChampionshipViewController.h"
#import "QAServerSideManager.h"
#import "QANetworkClient.h"
#import "QANetworkConfiguration.h"
#import "QAGetChampionshipQuery.h"
#import "QAGetChampionshipQuery.h"
#import "QAChampionship.h"
#import "QAPostHistoryQuery.h"
#import "QARouter.h"
#import "QADataManager.h"
#import "QAUserDefaultsManager.h"
#import "QAGetHistoryQuery.h"

@interface QAChampionshipPageViewController () <QAChampionshipViewControllerDelegate>

@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) UIButton * timerButton;

@property (nonatomic, strong) NSMutableArray *requestsArray;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger seconds;

@property (nonatomic, strong) QAChampionship *championship;
@property (nonatomic, strong) QAGetHistoryQuery *historyQuery;

@end

@implementation QAChampionshipPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setRequestsArray:[[NSMutableArray alloc] init]];
    [self setPageControl];
    [self setPageViewController:[[self storyboard] instantiateViewControllerWithIdentifier:@"QAChampionshipPageController"]];
    
    [self getChampionshipQuestions];
    
    for (UIGestureRecognizer *recognizer in [[self pageViewController] gestureRecognizers])
    {
        [recognizer setEnabled:NO];
    }
    [self setTimer];
    [self createTimerButton];
}

- (void)setPageControl
{
    UIPageControl *pageControl = [UIPageControl appearance];
    [pageControl setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [pageControl setCurrentPageIndicatorTintColor:[UIColor blackColor]];
    [pageControl setBackgroundColor:[UIColor whiteColor]];
}

- (void)getChampionshipQuestions
{
    QAGetChampionshipQuery *getChampionship = [[QAGetChampionshipQuery alloc] init];
    [getChampionship setOffset:@"1"];
    
    [[self dataManager] getChampionshipQuestionsWithQuery:getChampionship
                                             successBlock:^(id responseObject)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf setChampionship:responseObject[NSStringFromClass([QAChampionship class])]];
                                                    [weakSelf createPageViewControllerWithIndexPage:0];
                                                }
                                             failureBlock:^(QAError *theError)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf showAlertWithTitle:@"Error"
                                                                         message:[[theError userInfo] objectForKey:@"error"]
                                                              successActionBlock:^(id successAction)
                                                                {
                                                                    [weakSelf successAction];
                                                                }];
                                                }];
}

- (void)setTimer
{
    [self setSeconds:180];
    [self setTimer:[NSTimer scheduledTimerWithTimeInterval:1.0f
                                                    target:self
                                                  selector:@selector(updateTimerAction:)
                                                  userInfo:nil
                                                   repeats:YES]];
}

- (void)createTimerButton
{
    [self setTimerButton:[UIButton buttonWithType:UIButtonTypeCustom]];
    [[self timerButton] setFrame:CGRectMake(0, 0, 50, CGRectGetHeight([[self view] bounds]))];
    [[self timerButton] setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [[self timerButton] setExclusiveTouch:YES];
    [[self timerButton] setTitle:@"3:00" forState:UIControlStateNormal];
    [[self timerButton] setTitleColor:[UIColor whiteColor]
                             forState:UIControlStateNormal];
    UIBarButtonItem *backMenuBarButton = [[UIBarButtonItem alloc] initWithCustomView:[self timerButton]];
    [[self navigationItem] setLeftBarButtonItem:backMenuBarButton];
}

#pragma mark
#pragma mark - Create PageViewController

- (void)createPageViewControllerWithIndexPage:(NSInteger)indexPage
{
    if ([[[self championship] questions] count] > 0)
    {
        QAChampionshipViewController *startingViewController = [self viewControllerAtIndex:indexPage];
        [startingViewController setDelegate:self];
        NSArray *viewControllers = @[startingViewController];
        [[self pageViewController] setViewControllers:viewControllers
                                            direction:UIPageViewControllerNavigationDirectionForward
                                             animated:YES
                                           completion:nil];
        
        [[[self pageViewController] view] setFrame:CGRectMake(0, 0, CGRectGetWidth([[self view] frame]), CGRectGetHeight([[self view] frame]))];
        
        [self addChildViewController:[self pageViewController]];
        [[self view] addSubview:[[self pageViewController] view]];
        [[self pageViewController] didMoveToParentViewController:self];
    }
    else
    {
        [self showAlertWithTitle:@"Error"
                         message:@"Something went wrong"
              successActionBlock:^(id successAction)
                {
                    __weak typeof(self) weakSelf = self;
                    [weakSelf successAction];
                }];
    }
}

- (QAChampionshipViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([[[self championship] questions] count] == 0))
    {
        return nil;
    }
    QAQuestion *questionModel = [[[self championship] questions] objectAtIndex:index];
    
    QAChampionshipViewController *pageContentViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"QAChampionshipViewController"];
    [pageContentViewController setQuestion:questionModel];
    [pageContentViewController setPageIndex:index];
    
    return pageContentViewController;
}

#pragma mark
#pragma mark - QATestViewControllerDelegate

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary
{
    [[self requestsArray] addObject:dictionary];
    
    if ([[[self championship] questions] count] > indexPage)
    {
        [self createPageViewControllerWithIndexPage:indexPage];
    }
    else if ([[[self championship] questions] count] <= indexPage)
    {
        [self performPostHistoryRequest];
    }
}

- (void)performPostHistoryRequest
{
    if ([[self requestsArray] count] > 0)
    {
        QAPostHistoryQuery *postHistory = [[QAPostHistoryQuery alloc] init];
        [postHistory setQuestionsArray:[self requestsArray]];
        [postHistory setCategoryName:@"Championship"];
        [postHistory setDateInTimeInterval:[[NSDate date] timeIntervalSince1970]];
        
        [[self dataManager] postHistoryQuestionsWithQuery:postHistory
                                             successBlock:^(id responseObject)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf setHistoryQuery:[[QAGetHistoryQuery alloc] init]];
                                                    [[weakSelf dataManager] getHistoryQuestionsWithQuery:[weakSelf historyQuery]
                                                                                            successBlock:^(id responseObject)
                                                                                            {
                                                                                                [weakSelf showAlertWithTitle:@"Test is finished"
                                                                                                                     message:@"View You Result in History Screen"
                                                                                                          successActionBlock:^(id successAction)
                                                                                                            {
                                                                                                                [weakSelf successAction];
                                                                                                            }];
                                                                                            }
                                                                                            failureBlock:^(QAError *theError)
                                                                                            {
                                                                                                [weakSelf showAlertWithError:theError
                                                                                                          successActionBlock:^(id successAction)
                                                                                                            {
                                                                                                                [weakSelf successAction];
                                                                                                            }];
                                                                                            }];
                                                }
                                             failureBlock:^(QAError *theError)
                                                {
                                                    __weak typeof(self) weakSelf = self;
                                                    [weakSelf showAlertWithTitle:@"Error"
                                                                         message:[[theError userInfo] objectForKey:@"error"]
                                                              successActionBlock:^(id successAction)
                                                                {
                                                                    [weakSelf successAction];
                                                                }];
                                                }];
    }
}

#pragma mark
#pragma mark - Action

- (void)updateTimerAction:(NSTimer *)theTimer
{
    if ([self seconds] > 0)
    {
        self.seconds = self.seconds - 1;
        NSInteger minutes = (NSInteger)[self seconds] / 60;
        NSInteger seconds = [self seconds] % 60;
        [[self timerButton] setTitle:[NSString stringWithFormat:@"%ld:%02ld", (long)minutes, (long)seconds]
                            forState:UIControlStateNormal];
    }
    else
    {
        [self performPostHistoryRequest];
        [[self timer] invalidate];
        [self showAlertWithTitle:@"Finish"
                         message:@"Time is over"
              successActionBlock:^(id successAction)
                {
                    __weak typeof(self) weakSelf = self;
                    [weakSelf successAction];
                }];
    }
}

- (IBAction)stopAction:(UIBarButtonItem *)sender
{
    [self showAlertWithTwoButtonTitle:@"Stop test"
                              message:@"Stop this test?"
                   successActionBlock:^(id successAction)
                    {
                        __weak typeof(self) weakSelf = self;
                        [weakSelf successAction];
                    }];
}

#pragma mark - 
#pragma mark - Success Action

- (void)successAction
{
    [[self timer] invalidate];
    [self setTimer:nil];
    [[self router] popViewController];
}

@end
