//
//  QAChampionshipViewController.m
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAChampionshipViewController.h"
#import "QAChampionshipCell.h"
#import "QAQuestion.h"
#import "QAChampionshipHeaderView.h"
#import "QAChampionshipViewControllerDataSource.h"
#import "QAChampionshipTableViewDelegate.h"

@interface QAChampionshipViewController () <QAChampionshipDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) QAChampionshipViewControllerDataSource *championshipDataSource;
@property (nonatomic, strong) QAChampionshipTableViewDelegate *championshipDelegate;

@end

@implementation QAChampionshipViewController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self performTableView];
}

#pragma mark -
#pragma mark - performTableView

- (void)performTableView
{
    [self setChampionshipDataSource:[[QAChampionshipViewControllerDataSource alloc] initWithQuestion:[self question]]];
    [self setChampionshipDelegate:[[QAChampionshipTableViewDelegate alloc] initWithQuestion:[self question]]];
    
    [[self tableView] setDataSource:[self championshipDataSource]];
    [[self tableView] setDelegate:[self championshipDelegate]];
    [[self championshipDelegate]setDelegate:self];
    [[self tableView] registerNib:[UINib nibWithNibName:@"QAChampionshipCell" bundle:nil] forCellReuseIdentifier:@"QAChampionshipCell"];
}

#pragma mark
#pragma mark - QAChampionshipDelegate

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary
{
    if ([[self delegate] respondsToSelector:@selector(nextPage:requestsDictionary:)])
    {
        [[self delegate] nextPage:([self pageIndex] + 1)
               requestsDictionary:dictionary];
    }
}

@end
