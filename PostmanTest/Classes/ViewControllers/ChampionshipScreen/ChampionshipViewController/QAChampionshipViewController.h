//
//  QAChampionshipViewController.h
//  QuizApp
//
//  Created by Серега Лизунов on 10.10.16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QABaseViewController.h"

@class QAQuestion;

@protocol QAChampionshipViewControllerDelegate <NSObject>

- (void)nextPage:(NSInteger)indexPage requestsDictionary:(NSDictionary *)dictionary;

@end

@interface QAChampionshipViewController : QABaseViewController

@property (nonatomic, strong) QAQuestion *question;
@property (nonatomic, assign) NSUInteger pageIndex;

@property (nonatomic, weak) id<QAChampionshipViewControllerDelegate> delegate;

@end
