//
//  QAHistoryViewController.m
//  QuizApp
//
//  Created by Lizunov on 9/28/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "QAHistoryViewController.h"
#import "QAHistoryViewCell.h"
#import "QARouter.h"

#import "QAGetHistoryQuery.h"
#import "QAResultViewController.h"
#import "QADataManager.h"
#import "QAHistoriesCoreDataModel.h"
#import "QAHistoryViewControllerDataSource.h"
#import "QAUserDefaultsManager.h"
#import "QAHistoryTableViewDelegate.h"
#import "QAReachability.h"
#import "QAHistories.h"

@interface QAHistoryViewController () <QAHistoryDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) QAGetHistoryQuery *historyQuery;
@property (nonatomic, strong) QAHistories *histories;
@property (nonatomic, strong) QAHistory *history;

@property (nonatomic, strong) QAHistoryViewControllerDataSource *dataSource;
@property (nonatomic, strong) QAHistoryTableViewDelegate *delegate;

@end

@implementation QAHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationItem] setHidesBackButton:NO animated:YES];
    
    [self performSetHistories];
}

#pragma mark -
#pragma mark - Perform SetHistories

- (void)performSetHistories
{
    [self setHistoryQuery:[[QAGetHistoryQuery alloc] init]];
    [[self dataManager] getHistoryQuestionsWithQuery:[self historyQuery]
                                        successBlock:^(id responseObject)
                                        {
                                            __weak typeof(self) weakSelf = self;
                                            [weakSelf setHistories:responseObject[NSStringFromClass([QAHistories class])]];
                                            [weakSelf performTableView];
                                        }
                                        failureBlock:^(QAError *theError)
                                        {
                                            __weak typeof(self) weakSelf = self;
                                            [weakSelf showAlertWithError:theError
                                                      successActionBlock:^(id successAction)
                                                        {
                                                            [[weakSelf router] popViewController];
                                                        }];
                                        }];
}

#pragma mark -
#pragma mark - Set TableView

- (void)performTableView
{
    [self setDataSource:[[QAHistoryViewControllerDataSource alloc] initWithNetworkHistories:[self histories]]];
    [self setDelegate:[[QAHistoryTableViewDelegate alloc] initWithHistories:[self histories]]];
    
    [[self tableView] setDataSource:[self dataSource]];
    [[self tableView] setDelegate:[self delegate]];
    [[self delegate] setDelegate:self];
    [[self tableView] registerNib:[UINib nibWithNibName:@"QAHistoryViewCell" bundle:nil] forCellReuseIdentifier:@"QAHistoryViewCell"];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark - QAHistoryDelegate

- (void)performHistory:(QAHistory *)history
{
    [self setHistory:history];
    [[self router] showResultViewControllerWithHistory:history];
}

@end
