//
//  UIViewController+Category.m
//  QuizApp
//
//  Created by Lizunov on 9/27/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "UIViewController+Category.h"

@implementation UIViewController (Category)

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self popToRootViewController];
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
